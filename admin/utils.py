
from math import sin, cos, sqrt, atan2, radians
import random
import binascii
import os
import time
import string
import hashlib
from wiki_core.settings import ALLOWED_EXTENSIONS





#===========================================================================
#=========================generate_tracking_code============================
#===========================================================================
def allowed_file(filename):
	return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS
#===========================================================================
#===========================================================================






#===========================================================================
#===============================generate_token==============================
#===========================================================================
def generate_token(cellphone,backend_key=''):
    token = binascii.b2a_hex(os.urandom(15))
    return token
#===========================================================================
#===========================================================================






#===========================================================================
#=========================generate_tracking_code============================
#===========================================================================
def user_login_check(token):

    user_login = 0
    user_name = ''
    user_photo = ''

    try:
        this_user = Users_Db_Agahi.objects.filter(assigned_token = token).get()
        if this_user:
            user_login = 1
            user_name = this_user.firstname + ' ' + this_user.lastname
            user_photo = this_user.photo

    except:
        pass



    return user_login, user_name, user_photo
#===========================================================================
#===========================================================================




#===========================================================================
#=========================generate_tracking_code============================
#===========================================================================
def Banner_lister(class_key):

    Banner_list = []

    try:
        Mclass = Media_classes_Db.objects.filter(key = class_key).get()
        media_q = Banner_Db.objects.filter(media_class = Mclass).order_by('-id')[:5]
        print(media_q)
        for media in media_q:
            Banner_list.append({\
                "tracking_code" : media.tracking_code,\
                "title": media.title,\
                "cover": media.cover,\
                "url": media.url,\
                "key": media.key,\
                "media_class": media.media_class,\
            })
    except:
        pass


    return Banner_list
#===========================================================================
#===========================================================================







#===========================================================================
#=========================generate_tracking_code============================
#===========================================================================
def Media_lister(class_key):

    Media_list = []
    groupes_arrange = []
    groupe_list = set([])

    try:
        Mclass = Media_classes_Db.objects.filter(key = class_key).get()
        media_q = Media_Db.objects.filter(media_class = Mclass)
        if Mclass.groups_list:
            groupes_arrange = Mclass.groups_list.split(',')
        print(media_q)
        for media in media_q:
            Media_list.append({\
                "tracking_code" : media.tracking_code,\
                "title": media.title,\
                "date": media.date,\
                "group": media.group,\
                "cover": media.cover,\
                "note": media.note,\
                "is_free": media.is_free,\
                "media_class": media.media_class,\
            })

            if media.group is not "":
                if media.group not in groupes_arrange:
                    groupes_arrange.append(media.group)
    except:
        raise
        pass

    Media_list = sorted(Media_list, key=lambda x: x['date'], reverse=True)


    return Media_list, groupes_arrange
#===========================================================================
#===========================================================================


#===========================================================================
#=========================generate_tracking_code============================
#===========================================================================
def chosen_Media_lister(class_key):

    Media_list = []

    try:
        Mclass = Media_classes_Db.objects.filter(key = class_key).get()
        media_q = Media_Db.objects.filter(media_class = Mclass, chosen__gte = '2000-01-01 10:10').order_by('-chosen')[:10]
        print(media_q)
        for media in media_q:
            Media_list.append({\
                "tracking_code" : media.tracking_code,\
                "title": media.title,\
                "date": media.date,\
                "cover": media.cover,\
                "note": media.note,\
                "media_class": media.media_class,\
            })
    except:
        pass



    return Media_list
#===========================================================================
#===========================================================================





#===========================================================================
#=========================generate_tracking_code============================
#===========================================================================
def Class_lister(class_key):

    classes_list = []

    try:
        classes = Media_classes_Db.objects.filter().order_by('-date')

        for Class in classes:

            ispage = True if Class.key == class_key else False
            classes_list.append({\
                "name": Class.name,\
                "key": Class.key,\
                "icon": Class.icon,\
                "is_page": ispage,\
            })
    except:
        pass


    return classes_list
#===========================================================================
#===========================================================================
