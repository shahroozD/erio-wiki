from flask_mongoengine.wtf import model_form
from wiki_core.shared import db, login_manager
import uuid

# from flask_login import UserMixin


@login_manager.user_loader
def user_loader(username):
    # print(username)
    exist_user = User.objects(username=username).first()
    if exist_user:
        return exist_user
    return


@login_manager.request_loader
def request_loader(request):
    # email = request.form.get('email')
    # if email not in users:
    #     return
    #
    # user = Users_login()
    # user.id = email
    #
    # # DO NOT ever store passwords in plaintext and always compare password
    # # hashes using constant-time comparison!
    # user.is_authenticated = request.form['password'] == users[email]['password']

    return




# class Users_Tokens(db.EmbeddedDocument):
#     assigned_token = db.StringField(max_length=250)
#     assigned_token_stamps = db.StringField(max_length=50)
#     assigned_token_last_time = db.StringField(max_length=50)
#     device = db.StringField(max_length=50)

class User(db.Document):
    email = db.StringField(required=True)
    first_name = db.StringField(max_length=50)
    last_name = db.StringField(max_length=50)
    password = db.StringField()
    username = db.StringField(max_length=50)
    language = db.StringField(max_length=10)
    level = db.IntField()
    articles_langs = db.ListField()
    words_langs = db.ListField()
    create_date = db.StringField(max_length=50)
    is_active = db.BooleanField()
    is_authenticated = db.BooleanField()
    is_anonymous = db.BooleanField()
    creator = db.StringField(max_length=100)
    def get_id(self):
        # return uuid.uuid4().hex
        return self.username



class Info(db.EmbeddedDocument):
    key = db.StringField()
    value = db.StringField()
    link = db.StringField()

class Topic(db.EmbeddedDocument):
    title = db.StringField()
    info_table = db.ListField(db.EmbeddedDocumentField(Info))


class Category(db.Document):
    hashtag_name = db.StringField(max_length=120, required=True, )
    title = db.StringField(max_length=120, required=True, )
    root_name = db.StringField(max_length=120)
    description = db.StringField()
    lang = db.StringField(max_length=5)
    cover_url = db.StringField()
    author = db.ReferenceField(User)
    date = db.StringField()
    tags = db.ListField(db.StringField(max_length=30))
    topics = db.ListField(db.EmbeddedDocumentField(Topic))
    def get_id(self):
        # return uuid.uuid4().hex
        return self.hashtag_name

class Image(db.EmbeddedDocument):
    url = db.ListField(db.StringField())
    description = db.StringField()

class Content(db.EmbeddedDocument):
    title = db.StringField()
    text = db.StringField()
    image = db.ListField(db.EmbeddedDocumentField(Image))

class Old(db.EmbeddedDocument):
    root_name = db.StringField(max_length=120)
    author = db.ReferenceField(User)
    info_table = db.ListField(db.EmbeddedDocumentField(Info))
    cover_url = db.StringField()
    description = db.StringField()
    tags = db.ListField(db.StringField(max_length=30))
    content = db.ListField(db.EmbeddedDocumentField(Content))
    category = db.ReferenceField(Category)
    reference = db.ListField(db.StringField())

class Article(db.Document):
    hashtag_name = db.StringField(max_length=120, required=True, )
    title = db.StringField(max_length=120, required=True, )
    root_name = db.StringField(max_length=120)
    author = db.ReferenceField(User)
    info_table = db.ListField(db.EmbeddedDocumentField(Info))
    cover_url = db.StringField()
    description = db.StringField()
    date = db.StringField()
    chosen = db.StringField()
    old = db.ListField(db.EmbeddedDocumentField(Old))
    lang = db.StringField(max_length=5)
    tags = db.ListField(db.StringField(max_length=30))
    content = db.ListField(db.EmbeddedDocumentField(Content))
    category = db.ListField(db.StringField())
    num_visit = db.IntField(default=0)
    reference = db.ListField(db.StringField())
    abandon = db.BooleanField(default=False)
    def get_id(self):
        # return uuid.uuid4().hex
        return self.hashtag_name







class Languages(db.Document):
    name_id = db.StringField(max_length=5)
    name = db.StringField(max_length=50)
    show = db.StringField(max_length=50)
    unicode = db.BooleanField()
    rtl = db.BooleanField()
    create_date = db.StringField(max_length=50)
    is_active = db.BooleanField()
    def get_id(self):
        # return uuid.uuid4().hex
        return self.name_id







class word_meaning(db.EmbeddedDocument):
    author = db.StringField()
    meanings = db.ListField(db.StringField())
    lang = db.StringField(max_length=5)


class origin_word(db.EmbeddedDocument):
    word = db.StringField()
    pronounce = db.StringField()
    lang = db.StringField(max_length=5)
    meaning = db.StringField()
    next_origin = db.ListField(db.StringField())

class origin_tree_level(db.EmbeddedDocument):
    origin_words = db.ListField(db.EmbeddedDocumentField(origin_word))


class Old_Translate(db.EmbeddedDocument):
    author = db.ReferenceField(User)
    pronounce = db.StringField(max_length=120)
    origin = db.ListField(db.EmbeddedDocumentField(origin_tree_level))
    root_word = db.StringField(max_length=120)
    word_type = db.ListField(db.StringField())
    translations = db.ListField(db.EmbeddedDocumentField(word_meaning))
    date = db.StringField()
    synonyms = db.ListField(db.StringField())
    opposite = db.ListField(db.StringField())
    replaces = db.ListField(db.StringField(max_length=120))

class Words(db.Document):
    word = db.StringField(max_length=120, required=True)
    pronounce = db.StringField(max_length=120)
    origin = db.ListField(db.EmbeddedDocumentField(origin_tree_level))
    root_word = db.StringField(max_length=120)
    author = db.ReferenceField(User)
    type = db.ListField(db.StringField())
    word_type = db.ListField(db.StringField())
    translations = db.ListField(db.EmbeddedDocumentField(word_meaning))
    date = db.StringField()
    chosen = db.StringField()
    old = db.ListField(db.EmbeddedDocumentField(Old_Translate))
    lang = db.StringField(max_length=5)
    tags = db.ListField(db.StringField(max_length=120))
    synonyms = db.ListField(db.StringField())
    opposite = db.ListField(db.StringField())
    replaces = db.ListField(db.StringField(max_length=120))
    num_visit = db.IntField(default=0)
    abandon = db.BooleanField(default=False)
    verified = db.ReferenceField(User)
    def get_id(self):
        # return uuid.uuid4().hex
        return self.word





class Pages(db.Document):
    hashtag_name = db.StringField(max_length=120, required=True, )
    title = db.StringField(max_length=120, required=True, )
    last_editor = db.ReferenceField(User)
    cover_url = db.StringField(default="")
    description = db.StringField()
    date = db.StringField()
    lang = db.StringField(max_length=5)
    tags = db.ListField(db.StringField(max_length=30))
    content = db.ListField(db.EmbeddedDocumentField(Content))
    num_visit = db.IntField(default=0)
    def get_id(self):
        return self.hashtag_name
