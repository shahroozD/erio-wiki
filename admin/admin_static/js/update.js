var infoDeEditor = []
var infoEditor = []
var elementNum = 0
let rtlLangList = ['CLC', 'uCLC', '@LB', 'u@LB']
var contentLang
(rtlLangList.indexOf($('#article_lang').val()) >= 0)? contentLang = 'ar': contentLang = 'en'



$(document).on('click', ".btn-sm", function(){
  $( this).closest(".col-md-12").remove();
});



$('.content_box').each(function(index) {

    ClassicEditor.create( document.querySelector( '#editor'+index+'' ), {
            language: {
                // The UI will be English.
                ui: 'en',
                // But the content will be edited in Arabic.
                content: contentLang
            },
            toolbar: [ 'heading', '|', 'bold', 'italic', 'link', 'bulletedList', 'numberedList', 'blockQuote', "insertTable", "undo", "redo" ],
            removePlugins: [ 'ImageUpload', 'MediaEmbed' ],
            heading: {
                options: [
                    { model: 'paragraph', title: 'BKYJYZR[OD', class: 'ck-heading_paragraph' },
                    { model: 'heading2', view: 'h2', title: 'NKLYAX', class: 'ck-heading_heading2' }
                ]
            }
        } )
      .then( editor => {
          console.log( 'Editor was initialized'+ index );
          infoDeEditor[index] = editor;
          index++;
      } ).catch( error => {
          console.error( error );
      } );

})








ClassicEditor.create( document.querySelector( '#main_description' ), {
          language: {
              // The UI will be English.
              ui: 'en',
              // But the content will be edited in Arabic.
              content: contentLang
          },
          toolbar: ['bold', 'italic', 'link', 'bulletedList', 'numberedList', 'blockQuote', "undo", "redo" ],
          removePlugins: [ 'ImageUpload', 'MediaEmbed' ],
          heading: {
              options: [
                  { model: 'paragraph', title: 'BKYJYZR[OD', class: 'ck-heading_paragraph' },
                  { model: 'heading2', view: 'h2', title: 'NKLYAX', class: 'ck-heading_heading2' }
              ]
          }
      } )
    .then( editor => {
        descEditor = editor;
    } ).catch( error => {
        console.log( error );
    } );




    $("#RootAr").change(function(){
        $('#cover_image').attr('src', '');
        $('#RootCoverBtn').data('rootCoverAccepted', 'false');
        (!$(this).find(':selected').data('cover'))? $('#RootCoverBtn').hide() : $('#RootCoverBtn').show();
        $('#RootCoverBtn').data('rootCover', $(this).find(':selected').data('cover'))
    });

    $( "#RootCoverBtn" ).click(function() {
      if (!$(this).data('rootCover')) {
        $(this).data('rootCover', $('#RootAr').find(':selected').data('cover'))
      }
      $("#send_cover").val('');
      $('#cover_image').attr('src', '/media/'+$(this).data('rootCover'));
      $('#RootCoverBtn').data('rootCoverAccepted', 'true')
    })

    $( "#removeCover" ).click(function() {
      $("#send_cover").val('');
      $('#cover_image').attr('src', '');
      $('#RootCoverBtn').data('rootCoverAccepted', 'false')
    })



    $( "#add_info" ).click(function() {
      $(' <div class="col-md-12" >'+
              '<div class="box">'+
                  '<div class="box-header">'+
                    "<h3 class='box-title'>@AUAI[[T'Z B[ODKL</h3>"+
                    '<div class="pull-right box-tools">'+
                      '<button type="button" class="btn btn-default btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove">'+
                        '<i class="fa fa-times"></i></button>'+
                    '</div>'+
                  '</div>'+
                  '<div class="box-body pad content_box">'+
                    '<div class="form-group ">'+
                      '<label for="exampleInputPassword1">M[LYAX</label>'+
                      '<input  dir="auto" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" type="" class="content_name form-control required" placeholder="M[LYAX">'+
                    '</div>'+

                    '<form id="editor_form_'+elementNum+'">'+
                      '<textarea class="content_description required" id="editor-second-'+elementNum+'"  placeholder="'+"GZ JAJZIA'[Z JZUKL@[ JKL @[Y BALZ JAL[X?"+ '"' +
                                'style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" ></textarea>'+
                    '</form>'+
                  '</div>'+
                '</div>'+
              '</div>').insertBefore( "#submit_box" );

          ClassicEditor.create( document.querySelector( '#editor-second-'+elementNum+'' ), {
                    removePlugins: [ 'ImageUpload', 'MediaEmbed' ],
                    heading: {
                        options: [
                            { model: 'paragraph', title: 'BKYJYZR[OD', class: 'ck-heading_paragraph' },
                            { model: 'heading2', view: 'h2', title: 'NKLYAX', class: 'ck-heading_heading2' }
                        ]
                    }
                } )
              .then( editor => {
                  console.log( 'Editor was initialized', editor );
                  infoEditor[elementNum] = editor;
                  elementNum++;
              } ).catch( error => {
                  console.error( error );
              } );


    });




    $('#submit_form').click(function() {

        if (descEditor.getData()) {
          $('#main_description').val(descEditor.getData())
        } else {
          $('#editor_desc').find('.ck-editor__editable_inline').addClass('required_error')
        }


        if (infoDeEditor.length) {
          for (var i = 0; i < infoDeEditor.length; i++) {
            if (infoDeEditor[i].getData()) {
              $('#editor'+i).val(infoDeEditor[i].getData())
            } else {
              $('#editor_form_'+i).find('.ck-editor__editable_inline').addClass('required_error')
            }
          }
        }


        if (infoEditor.length) {
          for (var i = 0; i < infoEditor.length; i++) {
            if (infoEditor[i].getData()) {
              $('#editor-second-'+i).val(infoEditor[i].getData())
            } else {
              $('#editor_form_'+i).find('.ck-editor__editable_inline').addClass('required_error')
            }
          }
        }



        let allSet = true
        $('.required').each(function(index) {
          if (!$(this).val()) {
            $(this).addClass('required_error')
            allSet = false
          }
        })
        if (allSet) {
          json = {}
          content = []
          json['hashtag_name'] = $('#hashtag_name').val().toUpperCase()
          json['name'] = $('#name').val().toUpperCase()
          json['categories'] = $('#articles_categories').val()
          json['main_description'] = $('#main_description').val().toUpperCase()
          json['content'] = []
          json['rootCover'] =  ($('#RootCoverBtn').data('rootCoverAccepted')=='true')? $('#RootCoverBtn').data('rootCover') : null
          json['root_name'] = $("#RootAr").val()
          $('.content_box').each(function() {
              json['content'].push({
                "title" : $(this).find('.content_name').val().toUpperCase(),
                "text" : $(this).find('.content_description').val().toUpperCase(),
              })

          })

          metadata = {}
          if ($("#send_cover").prop('files').length) {
            metadata['name'] = $('#cover_name').val().toUpperCase()
            metadata['license'] = $('#cover_license').val()
            metadata['source'] = $('#cover_source').val()
            metadata['author'] = $('#cover_author').val().toUpperCase()
          }


          $('#send_json').val(JSON.stringify(json))
          $('#cover_metadata_json').val(JSON.stringify(metadata))
          $('#send_form').submit();
        }

    })






    function readURL(input) {
        if (input.prop('files').length) {
          var reader = new FileReader();

          reader.onload = function(e) {
            $('#cover_image').attr('src', e.target.result);
          }

          reader.readAsDataURL(input.prop('files')[0]);
        }
      }

      $("#choose_cover").click(function() {

        let allSet = true
        $('.required_modal').each(function(index) {
          // console.log($(this).val());
          if (!$(this).val()) {
            console.log($(this).val());
            $(this).addClass('required_error')
            allSet = false
          }
        })
        if ($("#send_cover").prop('files').length) {
          if (allSet) {
            $('#choose_cover_modal').modal('toggle');
            readURL($("#send_cover"));
          }
        } else {
          $('#loadFileXml').addClass('required_error')
        }
      });



      $('.required_modal').change(function() {
        $(this).removeClass('required_error')
      });


    // $( "#jostar" ).add( "<p id='new'>new paragraph</p>" )
