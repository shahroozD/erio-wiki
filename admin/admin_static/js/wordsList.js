$("[data-hide]").on("click", function(){
    $("." + $(this).attr("data-hide")).hide();
});



    $('.delete_btn').click(function() {
        $('#delete_modal').modal('show');
        $('#delete_word_data').val($(this).parent().find( ".wordData" ).val())
        $('#delete_word_data').data('row',$(this).parents('tr'))
    })



    $('#delete_word').click(function() {

        $('#loader').fadeIn()
        json_data = JSON.parse($('#delete_word_data').val())
        $.ajax({
              type: "POST",
              // headers: {
              //     "X-CSRFToken": "{{ csrf_token() }}"
              //     },
              url: "/admin/api/remove_word",
              data: JSON.stringify({
                    word: json_data.word,
                    lang: json_data.lang
                  }),
              contentType: "application/json;charset=UTF-8",
              dataType: "json",
              async: true,
          })
          .done(function(response){
            $('#loader').fadeOut()
            $('#ajax-alert').addClass('alert-success')
            $('#ajax-alert').show()
            TVaje.row( $('#delete_word_data').data('row') ).remove().draw();
            $('#delete_modal').modal('hide');

          })
          .fail(function(jqXHR, textStatus) {
            $('#loader').fadeOut()
            $('#ajax-alert').addClass('alert-danger')
            $('#ajax-alert').show()
            $('#delete_modal').modal('hide');

          });

    })
