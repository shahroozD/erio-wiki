var infoEditor = []
var descEditor
var choosenLang = 'CL'
var elementNum = 0



function textarea_Unicode(lang){
  if(lang[0] == 'u') {
    $(".form-control").addClass('unicodeFonts')
    $(".ck-editor").addClass('unicodeFonts')
  }
  else{
    $(".form-control").removeClass('unicodeFonts')
    $(".ck-editor").removeClass('unicodeFonts')
  }
}



$(document).on('click', ".btn-sm", function(){
  $( this).closest(".col-md-12").remove();
});




$("#langChooser").change(function(){
  (this.value == "CL")? $('#rootSection').hide() : $('#rootSection').show();
  // let rtlLangList = ['CLC', 'uCLC', '@LB', 'u@LB'];
  // (rtlLangList.indexOf(this.value) >= 0)? contentLang = 'ar': contentLang = 'en'
});











$( "#add_topic" ).click(function() {
  $(' <div class="col-md-12" >'+
          '<div class="box box-primary">'+
              '<div class="box-header">'+
                "<h3 class='box-title'>@AUAI[[T'Z B[ODKL</h3>"+
                '<div class="pull-right box-tools">'+
                  '<button type="button" class="btn btn-default btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove">'+
                    '<i class="fa fa-times"></i></button>'+
                '</div>'+
              '</div>'+
              '<div class="box-body pad content_box row">'+
                '<div class="form-group col-md-4">'+
                  // '<label for="exampleInputPassword1">TZV[J</label>'+
                  '<input autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" type="" class="title form-control required" placeholder="TZV[J">'+
                '</div>'+

                '<div class="form-group col-md-4">'+
                  "<button type='button' id='' class='btn btn-info add_info"+elementNum+"' >+ @KQMPJKY'Z @AUAI[[T'Z B[ODKL</button>"+
                '</div>'+

                '<div class="box-body info col-md-12 row">'+
                  '<div class="form-group col-md-4">'+
                    '<label for="exampleInputPassword1">TZV[J</label>'+
                    '<input autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" type="" class="key form-control required" placeholder="TZV[J">'+
                  '</div>'+
                  '<div class="form-group col-md-4">'+
                    '<label for="exampleInputPassword1">XA[Z</label>'+
                    '<input autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" type="" class="value form-control required" placeholder="XA[Z">'+
                  '</div>'+
                  '<div class="form-group col-md-4">'+
                    '<label for="exampleInputPassword1">CZ[RKYJ</label>'+
                    '<input autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" type="" class="link form-control" placeholder="CZ[RKYJ BZ FPNDAL">'+
                  '</div>'+
                '</div>'+


                '<div class="info_table col-md-12">'+



                '</div>'+
              '</div>'+
            '</div>'+
          '</div>').insertBefore( "#submit_box" );


          $( ".add_info"+elementNum+"" ).click(function() {
            $(' <div class="col-md-12" >'+
                    '<div class="">'+
                        '<div class="box-header">'+
                          "<h3 class='box-title'>@AUAI[[T'Z B[ODKL</h3>"+
                          '<div class="pull-right box-tools">'+
                            '<button type="button" class="btn btn-default btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove">'+
                              '<i class="fa fa-times"></i></button>'+
                          '</div>'+
                        '</div>'+
                        '<div class="box-body info pad row">'+
                          '<div class="form-group col-md-4">'+
                            '<label for="exampleInputPassword1">TZV[J</label>'+
                            '<input autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" type="" class="key form-control required" placeholder="TZV[J">'+
                          '</div>'+
                          '<div class="form-group col-md-4">'+
                            '<label for="exampleInputPassword1">XA[Z</label>'+
                            '<input autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" type="" class="value form-control required" placeholder="XA[Z">'+
                          '</div>'+
                          '<div class="form-group col-md-4">'+
                            '<label for="exampleInputPassword1">CZ[RKYJ</label>'+
                            '<input autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" type="" class="link form-control" placeholder="CZ[RKYJ BZ FPNDAL">'+
                          '</div>'+
                        '</div>'+
                      '</div>'+
                    '</div>').insertBefore( $(this).parents(".content_box").find(".info_table").last() );

          });

          elementNum++;

});




    $('#submit_form').click(function() {



        let allSet = true
        $('.required').each(function(index) {
          if (!$(this).val()) {
            $(this).addClass('required_error')
            allSet = false
          }
        })


        if (allSet) {
          json = {}
          content = []
          json['lang'] = $('#langChooser').val()
          json['hashtag_name'] = $('#hashtag_name').val().toUpperCase()
          json['title'] = $('#main_title').val().toUpperCase()
          json['description'] = $('#description').val().toUpperCase()
          json['topics'] = []


          $('.content_box').each(function() {
              info = []
              $(this).find(".info").each(function() {
                content ={}
                content['key'] = $(this).find('.key').val()
                content['value'] = $(this).find('.value').val()
                content['link'] = $(this).find('.link').val()
                info.push(content)

              })
              json['topics'].push({
                title : $(this).find('.title').val(),
                info_table: info
              })

          })
          console.log(json);

          metadata = {}
          if ($("#send_cover").prop('files').length) {
            metadata['name'] = $('#cover_name').val().toUpperCase()
            metadata['license'] = $('#cover_license').val()
            metadata['source'] = $('#cover_source').val()
            metadata['author'] = $('#cover_author').val().toUpperCase()
          }


          $('#send_json').val(JSON.stringify(json))
          $('#cover_metadata_json').val(JSON.stringify(metadata))
          $('#send_form').submit();
        }

    })






    function readURL(input) {
        if (input.prop('files').length) {
          var reader = new FileReader();

          reader.onload = function(e) {
            $('#cover_image').attr('src', e.target.result);
          }

          reader.readAsDataURL(input.prop('files')[0]);
        }
      }

      $("#choose_cover").click(function() {

        let allSet = true
        $('.required_modal').each(function(index) {
          // console.log($(this).val());
          if (!$(this).val()) {
            console.log($(this).val());
            $(this).addClass('required_error')
            allSet = false
          }
        })
        if ($("#send_cover").prop('files').length) {
          if (allSet) {
            $('#choose_cover_modal').modal('toggle');
            readURL($("#send_cover"));
          }
        } else {
          $('#loadFileXml').addClass('required_error')
        }
      });



      $('.required_modal').change(function() {
        $(this).removeClass('required_error')
      });
