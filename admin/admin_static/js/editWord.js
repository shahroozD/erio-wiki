var infoEditor = []
var descEditor
var choosenLang = 'CL'
var elementNum = 0


$("[data-hide]").on("click", function(){
    $("." + $(this).attr("data-hide")).hide();
});


function textarea_Unicode(lang){
  if(lang[0] == 'u') {
    $(".form-control").addClass('unicodeFonts')
    $(".ck-editor").addClass('unicodeFonts')
  }
  else{
    $(".form-control").removeClass('unicodeFonts')
    $(".ck-editor").removeClass('unicodeFonts')
  }
}




$(document).on('click', ".btn-sm", function(){
  $( this).closest(".col-md-12").remove();
});



$("#set_new_dictation").click(function(){
  $('#loader').fadeIn()
  json = {
    'word' : $('#word').val().toUpperCase(),
    'new_dictation' : $('#new_dictation').val().toUpperCase(),
    'lang' : $('#langChooser').val()
  }
  $.ajax({
      url: '/admin/api/update_dictation',
      type: 'POST',
      data: JSON.stringify(json),
      async: false,
      cache: false,
      contentType: false,
      dataType: 'json',
      contentType: 'application/json',
      processData: false,
      // success: function (response) {
      //   alert(response);
      // }
  })
    .done(function(response){
      $('#change_dictation_modal').modal('toggle');
      $('#ajax-alert').removeClass('alert-danger');
      $('#ajax-alert').addClass('alert-success');
      $('#ajax-alert').show();
      window.location.replace("./"+ $('#new_dictation').val().toUpperCase());
    })
    .fail(function(jqXHR, textStatus) {
      $('#change_dictation_modal').modal('toggle');
      $('#ajax-alert').removeClass('alert-success');
      $('#ajax-alert').addClass('alert-danger');
      $('#ajax-alert').show();
      $('#loader').fadeOut();
      window.scrollTo({top: 0, behavior: 'smooth'});
    });
});


$("#wordType").change(function(){
    $('.typeOption').hide();
    if (this.value == "1" || this.value == "2" || this.value == "6" || this.value =="@") {
      $('.nounOptions').show();
    }
    else if (this.value == "5") {
      $('.verbOptions').show();

    }
});


$(".trans_lang").change(function(){
  console.log(($(this).find(':selected').data('unicode') == "True"));
  if ($(this).find(':selected').data('unicode') == "True") {
    $(this).closest(".meaning_box").addClass("unicodeFonts")
  }else{
    $(this).closest(".meaning_box").removeClass("unicodeFonts")
  }
});


$( ".add_replace" ).click(function() {
  $(' <div class="col-md-12" >'+
          '<div class="">'+
              '<div class="box-header">'+
                '<label for="exampleInputPassword1">'+wordMean+'</label>'+
                '<div class="pull-right box-tools">'+
                  '<button type="button" class="btn btn-default btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove">'+
                    '<i class="fa fa-times"></i></button>'+
                '</div>'+
              '</div>'+
              '<div class="box-body info pad row">'+
                '<div class="form-group col-md-12">'+

                  '<input autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" type="" class="replace_word form-control" placeholder="'+wordMean+'">'+
                '</div>'+
              '</div>'+
            '</div>'+
          '</div>').insertBefore( $(this).parents(".modal-body").find(".info_table").last() );
});


$( "#set_replaces" ).click(function() {
    replaces_list = []
    $("#replaces_modal").find(".info").each(function(i) {
      var value = $(this).find('.replace_word').val().trim().toUpperCase()
      if (value) {
        replaces_list.push(value)
      }else{
        $(this).closest(".col-md-12").remove();
      }
    })
    console.log(replaces_list)
    $( "#replace_words" ).data( "words", replaces_list );
    $( "#replace_words" ).val( replaces_list.join(", ") );
    $('#replaces_modal').modal('toggle');
});






$( ".add_meaning" ).click(function() {
  $(' <div class="col-md-12" >'+
          '<div class="">'+
              '<div class="box-header">'+
                '<label for="exampleInputPassword1">'+wordMean+'</label>'+
                '<div class="pull-right box-tools">'+
                  '<button type="button" class="btn btn-default btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove">'+
                    '<i class="fa fa-times"></i></button>'+
                '</div>'+
              '</div>'+
              '<div class="box-body info pad row">'+
                '<div class="form-group col-md-12">'+

                  '<input autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" type="" class="translation form-control required" placeholder="'+wordMean+'">'+
                '</div>'+
              '</div>'+
            '</div>'+
          '</div>').insertBefore( $(this).parents(".meaning_box").find(".info_table").last() );

});





$( "#add_meaning" ).click(function() {

  let chosen_lang = []
  let lang_option = ""

  for (var i = 0; i < languages_list.length; i++) {
    lang_option +=  '<option value="'+languages_list[i].name_id+'" data-unicode="'+languages_list[i].unicode+'">'+languages_list[i].show+'</option>'
  }

  $(' <div class="col-md-12" >'+
          '<div class="box box-primary">'+
              '<div class="box-header">'+
                "<h3 class='box-title'>@AUAI[[T'Z B[ODKL</h3>"+
                '<div class="pull-right box-tools">'+
                  '<button type="button" class="btn btn-default btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove">'+
                    '<i class="fa fa-times"></i></button>'+
                '</div>'+
              '</div>'+
              '<div class="box-body pad meaning_box row">'+
                '<div class="box-body info col-md-12">'+
                    '<div class="col-md-12 row">'+
                      '<div class="form-group col-md-4">'+
                        '<label for="exampleInputPassword1">'+WordDic+'</label>'+
                        '<input autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" type="" class="author form-control required" placeholder="'+WordDic+'">'+
                      '</div>'+

                      '<div id="verbNumber" class="form-group col-md-4" >'+
                        '<div class="form-group">'+
                        '<label for="exampleInputPassword1">'+langLable+'</label>'+
                          '<select id="" class="form-control trans_lang" style="width: 100%;">'+
                            lang_option+
                          "</select>"+
                        "</div>"+
                      '</div>' +

                      '<div class="form-group col-md-4">'+
                        '<div class="form-group">' +
                          '<label for="exampleInputPassword1" style="height: 13px; width: 1px;"></label>' +
                          "<button type='button' id='' class='btn btn-info add_info"+elementNum+" form-control' >+ "+addInfoBtn+"</button>" +
                        '</div>'+
                      '</div>'+
                    '</div>'+
                  '</div>'+

                '<div class="box-body info col-md-12">'+
                  '<div class="form-group col-md-12">'+
                    '<label for="exampleInputPassword1">'+wordMean+'</label>'+
                    '<input autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" type="" class="translation form-control required" placeholder="'+wordMean+'">'+
                  '</div>'+
                '</div>'+


                '<div class="info_table col-md-12">'+



                '</div>'+
              '</div>'+
            '</div>'+
          '</div>').insertBefore( "#submit_box" );


          $( ".add_info"+elementNum+"" ).click(function() {
            $(' <div class="col-md-12" >'+
                    '<div class="">'+
                        '<div class="box-header">'+
                          '<label for="exampleInputPassword1">'+wordMean+'</label>'+
                          '<div class="pull-right box-tools">'+
                            '<button type="button" class="btn btn-default btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove">'+
                              '<i class="fa fa-times"></i></button>'+
                          '</div>'+
                        '</div>'+
                        '<div class="box-body info pad row">'+
                          '<div class="form-group col-md-12">'+

                            '<input autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" type="" class="translation form-control required" placeholder="'+wordMean+'">'+
                          '</div>'+
                        '</div>'+
                      '</div>'+
                    '</div>').insertBefore( $(this).parents(".meaning_box").find(".info_table").last() );

          });

          elementNum++;

});




    $('#submit_form').click(function() {

        $('#loader').fadeIn()

        let allSet = true
        $('.required').each(function(index) {
          if (!$(this).val()) {
            $(this).addClass('required_error')
            allSet = false
          }else{
            $(this).removeClass('required_error')
          }
        })


        if (allSet) {
          json = {}
          meaning = []
          json['lang'] = $('#langChooser').val()
          json['word'] = $('#word').val().toUpperCase()
          json['verified'] = $('#verifiedSelect').val()
          if ($('#pronounce').val()) {
            json['pronounce'] = $('#pronounce').val().toUpperCase()
          }
          // json['pronounce'] = ($('#pronounce').val().toUpperCase())? $('#pronounce').val().toUpperCase():null
          json['root_word'] = $('#root_word').val().toUpperCase()
          json['replaces'] = $( "#replace_words" ).data( "words")
          json['wordType'] = [$('#wordType').val()]

          if (json['wordType'][0] == '1' || json['wordType'][0] == '2' || json['wordType'][0] == '6' || json['wordType'][0] == '@') {
            json['wordType'].push($("#nounNumberSelect").val())
            json['wordType'].push($("#nounMoodsSelect").val())
            json['wordType'].push($("#nounSexSelect").val())
          }else if (json['wordType'][0] == '5') {
            json['wordType'].push($("#verbNumberSelect").val())
            json['wordType'].push($("#verbTranIntranSelect").val())
            json['wordType'].push($("#verbPointViewSelect").val())
            json['wordType'].push($("#verbTimeSelect").val())
          }

          json['translations'] = []


          $('.meaning_box').each(function() {
              meaning ={}
              meaning['author'] = $(this).find('.author').val()
              meaning['lang'] = $(this).find('.trans_lang').val()
              meaning['meanings'] = []

              $(this).find(".info").each(function(i) {
                if (i > 0) {
                  meaning['meanings'].push($(this).find('.translation').val())
                }
              })
              json['translations'].push(meaning)
          })
          console.log(json);


          $.ajax({
              url: '/admin/api/update_word',
              type: 'POST',
              data: JSON.stringify(json),
              async: false,
              cache: false,
              contentType: false,
              dataType: 'json',
              contentType: 'application/json',
              processData: false,
              // success: function (response) {
              //   alert(response);
              // }
          })
            .done(function(response){

              $('#ajax-alert').removeClass('alert-danger')
              $('#ajax-alert').addClass('alert-success')
              $('#ajax-alert').show()
              $('#loader').fadeOut()
              window.scrollTo({top: 0, behavior: 'smooth'});
            })
            .fail(function(jqXHR, textStatus) {

              $('#ajax-alert').removeClass('alert-success')
              $('#ajax-alert').addClass('alert-danger')
              $('#ajax-alert').show()
              $('#loader').fadeOut()
              window.scrollTo({top: 0, behavior: 'smooth'});
            });

          // metadata = {}
          // if ($("#send_cover").prop('files').length) {
          //   metadata['name'] = $('#cover_name').val().toUpperCase()
          //   metadata['license'] = $('#cover_license').val()
          //   metadata['source'] = $('#cover_source').val()
          //   metadata['author'] = $('#cover_author').val().toUpperCase()
          // }
          //
          //
          // $('#send_json').val(JSON.stringify(json))
          // $('#cover_metadata_json').val(JSON.stringify(metadata))
          // $('#send_form').submit();
        }

    })






    function readURL(input) {
        if (input.prop('files').length) {
          var reader = new FileReader();

          reader.onload = function(e) {
            $('#cover_image').attr('src', e.target.result);
          }

          reader.readAsDataURL(input.prop('files')[0]);
        }
      }

      $("#choose_cover").click(function() {

        let allSet = true
        $('.required_modal').each(function(index) {
          // console.log($(this).val());
          if (!$(this).val()) {
            console.log($(this).val());
            $(this).addClass('required_error')
            allSet = false
          }
        })
        if ($("#send_cover").prop('files').length) {
          if (allSet) {
            $('#choose_cover_modal').modal('toggle');
            readURL($("#send_cover"));
          }
        } else {
          $('#loadFileXml').addClass('required_error')
        }
      });



      $('.required_modal').change(function() {
        $(this).removeClass('required_error')
      });
