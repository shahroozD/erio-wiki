var infoEditor = [];
var descEditor;
var choosenLang = 'CL';
let rtlLangList = ['CLC', 'uCLC', '@LB', 'u@LB'];

var elementNum = 0;


$("[data-hide]").on("click", function(){
    $("." + $(this).attr("data-hide")).hide();
});


$(document).on('click', ".btn-sm", function(){
  $( this).closest(".col-md-12").remove();
});


function textarea_Unicode(lang){
  if(lang[0] == 'u') {
    $(".form-control").addClass('unicodeFonts');
    $(".ck-editor").addClass('unicodeFonts');
  }
  else{
    $(".form-control").removeClass('unicodeFonts');
    $(".ck-editor").removeClass('unicodeFonts');
  };
};



function CreateEditor(lang){

  let rtlLangList = ['CLC', 'uCLC', '@LB', 'u@LB'];

  var contentLang;
  (lang == "CL")? $('#rootSection').hide() : $('#rootSection').show();

  (rtlLangList.indexOf(lang) >= 0)? contentLang = 'ar': contentLang = 'en';


  ClassicEditor.create( document.querySelector( '#main_description' ), {
            language: {
                // The UI will be English.
                ui: 'en',
                // But the content will be edited in Arabic.
                content: contentLang
            },
            toolbar: ['bold', 'italic', 'link', 'bulletedList', 'numberedList', 'blockQuote', "undo", "redo" ],
            removePlugins: [ 'ImageUpload', 'MediaEmbed' ],
            heading: {
                options: [
                    { model: 'paragraph', title: 'BKYJYZR[OD', class: 'ck-heading_paragraph' },
                    { model: 'heading2', view: 'h2', title: 'NKLYAX', class: 'ck-heading_heading2' }
                ]
            }
        } )
      .then( editor => {
          descEditor = editor;
          textarea_Unicode(lang);
      } ).catch( error => {
          console.log( error );
      } );


      elementNum = 0;
      infoEditor = [];

      $('.content_description').each(function( ) {
        // console.log(element);
        ClassicEditor.create( this, {
                  language: {
                      // The UI will be English.
                      ui: 'en',
                      // But the content will be edited in Arabic.
                      content: contentLang
                  },
                  toolbar: [ 'heading', '|', 'bold', 'italic', 'link', 'bulletedList', 'numberedList', 'blockQuote', "insertTable", "undo", "redo" ],
                  removePlugins: [ 'ImageUpload', 'MediaEmbed' ],
                  heading: {
                      options: [
                          { model: 'paragraph', title: 'BKYJYZR[OD', class: 'ck-heading_paragraph' },
                          { model: 'heading2', view: 'h2', title: 'NKLYAX', class: 'ck-heading_heading2' }
                      ]
                  }
              } )
            .then( editor => {
                infoEditor[elementNum] = editor;
                elementNum++;
                textarea_Unicode(lang)
            } ).catch( error => {
                console.log( error );
            } );
      });

}







$("#langChooser").change(function(){
  descEditor.destroy()
  infoEditor.forEach(element => element.destroy());
  CreateEditor(this.value)
});






$('#hashtag_name').on('change keydown paste input', function(){
  this.value = this.value.replace(/ /g, '_');
})


$( "#add_info" ).click(function() {
  $(' <div class="col-md-12" >'+
          '<div class="box">'+
              '<div class="box-header">'+
                "<h3 class='box-title'>@AUAI[[T'Z B[ODKL</h3>"+
                '<div class="pull-right box-tools">'+
                  '<button type="button" class="btn btn-default btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove">'+
                    '<i class="fa fa-times"></i></button>'+
                '</div>'+
              '</div>'+
              '<div class="box-body pad content_box">'+
                '<div class="form-group ">'+
                  '<label for="exampleInputPassword1">M[LYAX</label>'+
                  '<input  dir="auto" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" type="" class="content_name form-control required" placeholder="M[LYAX">'+
                '</div>'+

                '<form id="editor_form_'+elementNum+'">'+
                  '<textarea class="content_description required" id="editor'+elementNum+'"  placeholder="'+"GZ JAJZIA'[Z JZUKL@[ JKL @[Y BALZ JAL[X?"+ '"' +
                            'style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" ></textarea>'+
                '</form>'+
              '</div>'+
            '</div>'+
          '</div>').insertBefore( "#submit_box" );

      ClassicEditor.create( document.querySelector( '#editor'+elementNum+'' ), {
                toolbar: [ 'heading', '|', 'bold', 'italic', 'link', 'bulletedList', 'numberedList', 'blockQuote', "insertTable", "undo", "redo" ],
                removePlugins: [ 'ImageUpload', 'MediaEmbed' ],
                heading: {
                    options: [
                        { model: 'paragraph', title: 'BKYJYZR[OD', class: 'ck-heading_paragraph' },
                        { model: 'heading2', view: 'h2', title: 'NKLYAX', class: 'ck-heading_heading2' }
                    ]
                }
            } )
          .then( editor => {
              // console.log( 'Editor was initialized', editor );
              infoEditor[elementNum] = editor;
              elementNum++;
              textarea_Unicode($("#langChooser").val())
          } ).catch( error => {
              console.error( error );
          } );

});





    $('#submit_form').click(function() {

        if (descEditor.getData()) {
          $('#main_description').val(descEditor.getData())
        } else {
          $('#editor_desc').find('.ck-editor__editable_inline').addClass('required_error')
        }


        if (infoEditor.length) {
          for (var i = 0; i < infoEditor.length; i++) {
            if (infoEditor[i].getData()) {
              $('#editor'+i).val(infoEditor[i].getData())
            } else {
              $('#editor_form_'+i).find('.ck-editor__editable_inline').addClass('required_error')
            }
          }
        }

        let allSet = true
        $('.required').each(function(index) {
          if (!$(this).val()) {
            $(this).addClass('required_error')
            allSet = false
          }
        })
        if (allSet) {
          json = {}
          content = []
          json['lang'] = $('#langChooser').val()
          json['hashtag_name'] = $('#hashtag_name').val().toUpperCase().replace(/ /g,"_");
          json['title'] = $('#title').val().toUpperCase()
          json['main_description'] = $('#main_description').val().toUpperCase()
          json['content'] = []

          $('.content_box').each(function() {
              json['content'].push({
                "title" : $(this).find('.content_name').val().toUpperCase(),
                "text" : $(this).find('.content_description').val().toUpperCase(),
              })

          })




          $('#loader').fadeIn()
          $.ajax({
                type: "POST",
                // headers: {
                //     "X-CSRFToken": "{{ csrf_token() }}"
                //     },
                url: "/admin/api/add_page",
                data: JSON.stringify(json),
                contentType: "application/json;charset=UTF-8",
                dataType: "json",
                async: true,
            })
            .done(function(response){
              $('#loader').fadeOut()
              $('#ajax-alert').removeClass('alert-danger')
              $('#ajax-alert').addClass('alert-success')
              $('#ajax-alert').show()
              $('#delete_modal').modal('hide');
            })
            .fail(function(jqXHR, textStatus) {
              $('#loader').fadeOut()
              $('#ajax-alert').removeClass('alert-success')
              $('#ajax-alert').addClass('alert-danger')
              $('#ajax-alert').show()
              $('#delete_modal').modal('hide');
            });

        }


    })






      $('.required_modal').change(function() {
        $(this).removeClass('required_error')
      });
