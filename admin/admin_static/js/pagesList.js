    $("[data-hide]").on("click", function(){
        $("." + $(this).attr("data-hide")).hide();
    });



    $('.delete_btn').click(function() {
        $('#delete_modal').modal('show');
        $('#delete_hashtag_name').val($(this).parent().find( ".hashtagName" ).val())
        $('#delete_hashtag_name').data('row',$(this).parents('tr'))
    })



    $('#delete_page').click(function() {

        $('#loader').fadeIn()
        json_data = JSON.parse($('#delete_hashtag_name').val())
        $.ajax({
              type: "POST",
              // headers: {
              //     "X-CSRFToken": "{{ csrf_token() }}"
              //     },
              url: "/admin/api/remove_page",
              data: JSON.stringify({
                    hashtag_name: json_data.hashtag_name,
                    lang: json_data.lang
                  }),
              contentType: "application/json;charset=UTF-8",
              dataType: "json",
              async: true,
          })
          .done(function(response){
            $('#loader').fadeOut()
            $('#ajax-alert').removeClass('alert-danger')
            $('#ajax-alert').addClass('alert-success')
            $('#ajax-alert').show()
            TPage.row( $('#delete_hashtag_name').data('row') ).remove().draw();
            $('#delete_modal').modal('hide');

          })
          .fail(function(jqXHR, textStatus) {
            $('#loader').fadeOut()
            $('#ajax-alert').removeClass('alert-success')
            $('#ajax-alert').addClass('alert-danger')
            $('#ajax-alert').show()
            $('#delete_modal').modal('hide');

          });

    })
