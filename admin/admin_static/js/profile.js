var infoEditor = []
var descEditor
var choosenLang = 'CL'
let rtlLangList = ['CLC', 'uCLC', '@LB', 'u@LB']

var elementNum = 0


$("[data-hide]").on("click", function(){
    $("." + $(this).attr("data-hide")).hide();
});





$('#pass_modal').click(function() {
    $('#reset_pass_modal').modal('show')
})


$('#edit_user').click(function() {
  $( ".userdata" ).prop( "disabled", false );
  $('.select2').prop('disabled', false);
  $('#primary-btns').hide()
  $('#edit-btns').show()

})

let cancel = () => {
  $( ".userdata" ).prop( "disabled", true );
  $('.select2').prop('disabled', true);
  $( ".userdata" ).each(function( index ) {
    $(this).val($(this).data( "default" ))
  })
  $( ".select2" ).each(function( index ) {
    $(this).val($(this).data( "default" )).change();
  })

  $('#edit-btns').hide()
  $('#primary-btns').show()
}

$('#cancel').click(function() {
  cancel()

})



$('.delete_btn').click(function() {
    $('#delete_modal').modal('show');
    // $(this).parent().find( ".hashtagName" ).val()
    // console.log($(this).parent().find( ".hashtagName" ).val());
    $('#delete_hashtag_name').val($(this).parent().find( ".hashtagName" ).val())
    $('#delete_hashtag_name').data('row',$(this).parents('tr'))
})


$('#delete_article').click(function() {

  $('#loader').fadeIn()
  $.ajax({
        type: "POST",
        // headers: {
        //     "X-CSRFToken": "{{ csrf_token() }}"
        //     },
        url: "/admin/api/remove_article",
        data: JSON.stringify({
              hashtag_name: $('#delete_hashtag_name').val(),
            }),
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        async: true,
    })
    .done(function(response){
      $('#loader').fadeOut()
      $('#ajax-alert').addClass('alert-success')
      $('#ajax-alert').show()
      TArticle.row( $('#delete_hashtag_name').data('row') ).remove().draw();
      $('#delete_modal').modal('hide');

    })
    .fail(function(jqXHR, textStatus) {
      $('#loader').fadeOut()
      $('#ajax-alert').addClass('alert-danger')
      $('#ajax-alert').show()
      $('#delete_modal').modal('hide');

    });




})




$('#submit_edit').click(function() {

  $('#loader').fadeIn()
  $.ajax({
        type: "POST",
        // headers: {
        //     "X-CSRFToken": "{{ csrf_token() }}"
        //     },
        url: "/admin/api/update_profile",
        data: JSON.stringify({
              first_name: $('#first_name').val(),
              last_name: $('#last_name').val(),
              username: $('#user_name').val(),
              language: $('#UILang').val(),
              Jlevel: $('#JLevel').val(),
              Wlevel: $('#WLevel').val(),
              articles_langs: $('#articles_allowed_lang').val(),
              words_langs: $('#words_allowed_lang').val(),
            }),
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        async: true,
    })
    .done(function(response){
      $('#loader').fadeOut()
      $('#ajax-alert').addClass('alert-success')
      $('#ajax-alert').show()
      $( ".userdata" ).prop( "disabled", true );
      $('.select2').prop('disabled', true);
      $('#edit-btns').hide()
      $('#primary-btns').show()

    })
    .fail(function(jqXHR, textStatus) {
      $('#loader').fadeOut()
      $('#ajax-alert').addClass('alert-danger')
      $('#ajax-alert').show()
      cancel()

    });
})






$('#set_pass_btn').click(function() {

  var allSet = true
  $('.pass_required').each(function(index) {
    if (!$(this).val()) {
      $(this).addClass('required_error')
      allSet = false
    }
  })
  if (allSet) {
    $('#loader').fadeIn()
      $.ajax({
        url: "/admin/api/change_pass",
        type: "POST",
        data: JSON.stringify({
              username: $('#user_name').val(),
              new_pass: $('#pass').val(),
            }),
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        async: true
      })
      .done(function(msg) {
        $('#loader').fadeOut()
        $('#ajax-alert').addClass('alert-success')
        $('#ajax-alert').show()
        $('.modal').modal('hide')
        cancel()
      })
      .fail(function(jqXHR, textStatus) {
        $('#loader').fadeOut()
        $('#ajax-alert').addClass('alert-danger')
        $('#ajax-alert').show()
        $('.modal').modal('hide')
        cancel()
      });
  }
})


// ===================================
