import json
from Shahi import *







#===========================================================================
#========================= validate_user_login =============================
#===========================================================================
def validate_login(request):
    error_codes = []
    warning_codes = []
    success_flag = 1


    #-------------------------Validate the Details--------------------------

    try:
        username = request.form['username']
        if not (username):
            error_codes.append('1')
            success_flag = 0
    except:
        username = ''
        error_codes.append('1')
        success_flag = 0



    try:
        password = request.form['password']
        if not (password):
            error_codes.append('1')
            success_flag = 0
    except:
        password = ''
        error_codes.append('1')
        success_flag = 0

    return username,password,error_codes,success_flag



#===========================================================================
#===========================================================================




#===========================================================================
#======================== validate_add_category ============================
#===========================================================================
def validate_add_category(request):
    error_codes = []
    warning_codes = []
    success_flag = 1
    code_length = 8

    try:
        input_data = json.loads(request.form['json'])
    except:
        input_data = {}


    try:
        input_metadata = json.loads(request.form['metadata'])
        cover = request.files['cover']




        # img = Image.open(cover)
        # print("========----------=========")
        # print(cover)
        # img.save( img.filename ,format="JPEG", quality=70)
        # print(img)
        # # cover = img
        # thumb_io = StringIO.StringIO()
        # thumb.save(thumb_io, format='JPEG')
        # cover = InMemoryUploadedFile(img, None, img.filename, 'image/jpeg',None)
    except:
        # raise
        input_metadata ={}
        cover = ""




    #-------------------------Validate the Details--------------------------

    try:
        lang = input_data['lang']
        # if note is None and this_type == '':
            # lang = "CL"
    except:
        lang = 'CL'



    try:
        hashtag_name = input_data['hashtag_name']
        # if note is None and this_type == 'setmedia':
        #     error_codes.append('1')
        #     success_flag = 0

    except:
        hashtag_name = ''
        error_codes.append('1')
        success_flag = 0


    try:
        title = input_data['title']
        # if note is None and this_type == 'setmedia':
        #     error_codes.append('1')
        #     success_flag = 0

    except:
        title = ''
        error_codes.append('1')
        success_flag = 0



    try:
        description = input_data['description']
        # if note is None and this_type == 'setmedia':
        #     error_codes.append('1')
        #     success_flag = 0

    except:
        description = ''
        error_codes.append('1')
        success_flag = 0



    try:
        for info in input_data['topics']:
            pass
        topics = input_data['topics']
        # if note is None and this_type == 'setmedia':
        #     error_codes.append('1')
        #     success_flag = 0

    except:
        topics = []
        error_codes.append('1')
        success_flag = 0





    return lang,hashtag_name,title,description,topics,cover,input_metadata,error_codes,success_flag



#===========================================================================
#===========================================================================






#===========================================================================
#========================= validate_user_login =============================
#===========================================================================
def validate_add_article(request):
    error_codes = []
    warning_codes = []
    success_flag = 1
    code_length = 8

    try:
        input_data = json.loads(request.form['json'])
    except:
        input_data = {}


    try:
        input_metadata = json.loads(request.form['metadata'])
        cover = request.files['cover']




        # img = Image.open(cover)
        # print("========----------=========")
        # print(cover)
        # img.save( img.filename ,format="JPEG", quality=70)
        # print(img)
        # # cover = img
        # thumb_io = StringIO.StringIO()
        # thumb.save(thumb_io, format='JPEG')
        # cover = InMemoryUploadedFile(img, None, img.filename, 'image/jpeg',None)
    except:
        # raise
        input_metadata ={}
        cover = ""




    #-------------------------Validate the Details--------------------------

    try:
        lang = input_data['lang']
        # if note is None and this_type == '':
        #     lang = "CL",
    except:
        lang = 'CL'


    try:
        categories = input_data['categories']
        # if note is None and this_type == '':
        #     lang = "CL"categories,
    except:
        categories = []



    try:
        rootCover = input_data['rootCover']
    except:
        rootCover = ''


    try:
        root_name = input_data['root_name']
    except:
        root_name = ''



    try:
        hashtag_name = input_data['hashtag_name']
        # if note is None and this_type == 'setmedia':
        #     error_codes.append('1')
        #     success_flag = 0

    except:
        hashtag_name = ''
        error_codes.append('1')
        success_flag = 0


    try:
        name = input_data['name']
        # if note is None and this_type == 'setmedia':
        #     error_codes.append('1')
        #     success_flag = 0

    except:
        name = ''
        error_codes.append('1')
        success_flag = 0



    try:
        main_description = input_data['main_description']
        # if note is None and this_type == 'setmedia':
        #     error_codes.append('1')
        #     success_flag = 0

    except:
        main_description = ''
        error_codes.append('1')
        success_flag = 0



    try:
        for info in input_data['content']:
            pass
        content = input_data['content']
        # if note is None and this_type == 'setmedia':
        #     error_codes.append('1')
        #     success_flag = 0

    except:
        content = []
        error_codes.append('1')
        success_flag = 0





    return lang,categories,hashtag_name,name,main_description,content,root_name,rootCover,cover,input_metadata,error_codes,success_flag



#===========================================================================
#===========================================================================



#===========================================================================
#========================= validate_user_login =============================
#===========================================================================
def validate_update_article(request):
    error_codes = []
    warning_codes = []
    success_flag = 1
    code_length = 8

    try:
        input_data = json.loads(request.form['json'])
    except:
        input_data = {}


    try:
        input_metadata = json.loads(request.form['metadata'])
        cover = request.files['cover']




        # img = Image.open(cover)
        # print("========----------=========")
        # print(cover)
        # img.save( img.filename ,format="JPEG", quality=70)
        # print(img)
        # # cover = img
        # thumb_io = StringIO.StringIO()
        # thumb.save(thumb_io, format='JPEG')
        # cover = InMemoryUploadedFile(img, None, img.filename, 'image/jpeg',None)
    except:
        # raise
        input_metadata ={}
        cover = ""




    #-------------------------Validate the Details--------------------------

    try:
        hashtag_name = input_data['hashtag_name']
        # if note is None and this_type == 'setmedia':
        #     error_codes.append('1')
        #     success_flag = 0

    except:
        hashtag_name = ''
        error_codes.append('1')
        success_flag = 0






    try:
        name = input_data['name']
        # if note is None and this_type == 'setmedia':
        #     error_codes.append('1')
        #     success_flag = 0

    except:
        name = ''
        error_codes.append('1')
        success_flag = 0


    try:
        categories = input_data['categories']
        # if note is None and this_type == '':
        #     lang = "CL"categories,
    except:
        categories = []



    try:
        main_description = input_data['main_description']
        # if note is None and this_type == 'setmedia':
        #     error_codes.append('1')
        #     success_flag = 0

    except:
        main_description = ''
        error_codes.append('1')
        success_flag = 0



    try:
        for info in input_data['content']:
            pass
        content = input_data['content']
        # if note is None and this_type == 'setmedia':
        #     error_codes.append('1')
        #     success_flag = 0

    except:
        content = []
        error_codes.append('1')
        success_flag = 0


    try:
        root_name = input_data['root_name']
    except:
        root_name = ''


    try:
        rootCover = input_data['rootCover']
    except:
        rootCover = ''






    return hashtag_name,name,categories,main_description,content,root_name,rootCover,cover,input_metadata,error_codes,success_flag



#===========================================================================
#===========================================================================





#===========================================================================
#========================= validate_add_class =============================
#===========================================================================
def validate_add_class(request):
    error_codes = []
    warning_codes = []
    success_flag = 1
    code_length = 8

    try:
        input_data = json.loads(request.POST)
    except:
        input_data = {}


    try:
        icon = request.FILES['icon']
    except:
        icon = ""


    #-------------------------Validate the Details--------------------------

    try:
        this_type = request.POST['type']

    except:
        this_type = ''

    try:
        name = request.POST['name']
        if name is None  :
            error_codes.append('1')
            success_flag = 0

    except:
        name = ''
        error_codes.append('1')
        success_flag = 0



    try:
        key = request.POST['key']
        if key is None  :
            error_codes.append('1')
            success_flag = 0

    except:
        key = ''
        error_codes.append('1')
        success_flag = 0




    return this_type,name,icon,key,error_codes,success_flag



#===========================================================================
#===========================================================================





#===========================================================================
#========================= validate_user_login =============================
#===========================================================================
def validate_add_medi4a(request):
    error_codes = []
    warning_codes = []
    success_flag = 1


    try:
        input_data = json.loads(request.body)
    except:
        input_data = {}

    #-------------------------Validate the Details--------------------------

    try:
        device_id = input_data['username']
        if not (device_id):
            error_codes.append('1')
            success_flag = 0
    except:
        device_id = ''
        error_codes.append('1')
        success_flag = 0


    return device_id,error_codes,success_flag



#===========================================================================
#===========================================================================




#===========================================================================
#========================= validate_user_login =============================
#===========================================================================
def validate_delete_article(request):
    error_codes = []
    warning_codes = []
    success_flag = 1


    try:
        input_data = json.loads(request.body)
    except:
        input_data = {}

    #-------------------------Validate the Details--------------------------

    try:
        article_hashtag = request.form['article_hashtag']
        if not (article_hashtag):
            error_codes.append('1')
            success_flag = 0
    except:
        article_hashtag = ''
        error_codes.append('1')
        success_flag = 0



    try:
        type = request.form['type']
        if not (type):
            error_codes.append('1')
            success_flag = 0
    except:
        type = ''
        error_codes.append('1')
        success_flag = 0


    return article_hashtag,type,error_codes,success_flag



#===========================================================================
#===========================================================================




#===========================================================================
#========================= validate_add_class =============================
#===========================================================================
def validate_add_word(request):
    error_codes = []
    warning_codes = []
    success_flag = 1


    try:
        input_data = request.json
    except:
        raise
        input_data = {}

    #-------------------------Validate the Details--------------------------
    # print(input_data)
    try:
        word = input_data['word']
        if not (word):
            error_codes.append('0')
            success_flag = 0
    except:
        # raise
        word = ''
        error_codes.append('01')
        success_flag = 0



    try:
        pronounce = input_data['pronounce']
        if not (pronounce):
            # error_codes.append('2')
            # success_flag = 0
            pronounce = ''
    except:
        pronounce = ''
        # error_codes.append('1')
        # success_flag = 0


    try:
        lang = input_data['lang']
        if not (lang):
            error_codes.append('3')
            success_flag = 0
    except:
        lang = ''




    try:
        root_word = input_data['root_word']
        if not (root_word):
            root_word = ''
    except:
        root_word = ''


    try:
        word_type = input_data['wordType']
        if not (word_type):
            # error_codes.append('5')
            # success_flag = 0
            word_type = []
    except:
        word_type = []




    try:
        translations = input_data['translations']
        if not (translations):
            error_codes.append('6')
            success_flag = 0
    except:
        translations = ''
        error_codes.append('06')
        success_flag = 0



    try:
        replaces = input_data['replaces']
        if not (replaces):
            # error_codes.append('7')
            # success_flag = 0
            replaces = []
    except:
        replaces = []







    return word,pronounce,lang,root_word,word_type,translations,replaces,error_codes,success_flag



#===========================================================================
#===========================================================================




#===========================================================================
#========================= validate_add_class =============================
#===========================================================================
def validate_edit_word(request):
    error_codes = []
    warning_codes = []
    success_flag = 1


    try:
        input_data = request.json
    except:
        raise
        input_data = {}

    #-------------------------Validate the Details--------------------------
    # print(input_data)
    try:
        word = input_data['word']
        if not (word):
            error_codes.append('0')
            success_flag = 0
    except:
        # raise
        word = ''
        error_codes.append('01')
        success_flag = 0



    try:
        verified = input_data['verified']
        if not (verified) or verified=="0":
            verified = False
        else:
            verified = True
    except:
        # raise
        verified = False
        # error_codes.append('01')
        # success_flag = 0



    try:
        pronounce = input_data['pronounce']
        if not (pronounce):
            # error_codes.append('2')
            # success_flag = 0
            pronounce = ''
    except:
        pronounce = ''
        # error_codes.append('1')
        # success_flag = 0


    try:
        lang = input_data['lang']
        if not (lang):
            error_codes.append('3')
            success_flag = 0
    except:
        lang = ''




    try:
        root_word = input_data['root_word']
        if not (root_word):
            root_word = ''
    except:
        root_word = ''


    try:
        word_type = input_data['wordType']
        if not (word_type):
            error_codes.append('5')
            success_flag = 0
    except:
        word_type = []




    try:
        translations = input_data['translations']
        if not (translations):
            error_codes.append('6')
            success_flag = 0
    except:
        translations = ''
        error_codes.append('06')
        success_flag = 0



    try:
        replaces = input_data['replaces']
        if not (replaces):
            # error_codes.append('7')
            # success_flag = 0
            replaces = []
    except:
        replaces = []







    return word,verified,pronounce,lang,root_word,word_type,translations,replaces,error_codes,success_flag



#===========================================================================
#===========================================================================


#===========================================================================
#========================= validate_add_class =============================
#===========================================================================
def validate_new_dictation(request):
    error_codes = []
    warning_codes = []
    success_flag = 1


    try:
        input_data = request.json
    except:
        raise
        input_data = {}

    #-------------------------Validate the Details--------------------------
    # print(input_data)
    try:
        word = input_data['word']
        if not (word):
            error_codes.append('0')
            success_flag = 0
    except:
        # raise
        word = ''
        error_codes.append('01')
        success_flag = 0



    try:
        new_dictation = input_data['new_dictation']
        if not (new_dictation):
            error_codes.append('02')
            success_flag = 0
    except:
        # raise
        new_dictation = ''
        error_codes.append('3')
        success_flag = 0


    try:
        lang = input_data['lang']
        if not (lang):
            error_codes.append('3')
            success_flag = 0
    except:
        lang = ''




    return word,new_dictation,lang,error_codes,success_flag


#===========================================================================
#===========================================================================





#===========================================================================
#========================= validate_user_login =============================
#===========================================================================
def validate_add_user(request):
    error_codes = []
    warning_codes = []
    success_flag = 1


    try:
        input_data = json.loads(request.body)
    except:
        input_data = {}

    #-------------------------Validate the Details--------------------------

    try:
        username = request.form['user_name']
        if not (username):
            error_codes.append('1')
            success_flag = 0
    except:
        username = ''
        error_codes.append('1')
        success_flag = 0



    try:
        FName = request.form['first_name']
        if not (FName):
            error_codes.append('1')
            success_flag = 0
    except:
        FName = ''
        error_codes.append('1')
        success_flag = 0



    try:
        LName = request.form['last_name']
        if not (LName):
            error_codes.append('1')
            success_flag = 0
    except:
        LName = ''
        error_codes.append('1')
        success_flag = 0



    try:
        lang = request.form['lang']
        if not (lang):
            error_codes.append('1')
            success_flag = 0
    except:
        lang = ''
        error_codes.append('1')
        success_flag = 0



    try:
        VLevel = request.form['VLevel']
        if not (VLevel):
            error_codes.append('1')
            success_flag = 0
    except:
        VLevel = ''
        error_codes.append('1')
        success_flag = 0



    try:
        JLevel = request.form['JLevel']
        if not (JLevel):
            error_codes.append('1')
            success_flag = 0
    except:
        JLevel = ''
        error_codes.append('1')
        success_flag = 0



    try:
        articles_allowed_lang = request.form['articles_allowed_lang[]']
        if not (articles_allowed_lang):
            error_codes.append('1')
            success_flag = 0
    except:
        articles_allowed_lang = ''
        error_codes.append('1')
        success_flag = 0


    try:
        words_allowed_lang = request.form['words_allowed_lang[]']
        if not (words_allowed_lang):
            error_codes.append('1')
            success_flag = 0
    except:
        words_allowed_lang = ''
        error_codes.append('1')
        success_flag = 0




    try:
        password = request.form['pass']
        if not (password):
            error_codes.append('1')
            success_flag = 0
    except:
        password = ''
        error_codes.append('1')
        success_flag = 0



    return username,FName,LName,lang,VLevel,JLevel,articles_allowed_lang,words_allowed_lang,password,error_codes,success_flag



#===========================================================================
#===========================================================================






#===========================================================================
#========================= validate_user_login =============================
#===========================================================================
def validate_langs_list(request):
    error_codes = []
    warning_codes = []
    success_flag = 1


    try:
        input_data = json.loads(request.body)
    except:
        input_data = {}

    #-------------------------Validate the Details--------------------------

    try:
        name_id = request.form['name_id']
        print(name_id)
        if not (name_id):
            error_codes.append('0')
            success_flag = 0
    except:
        raise
        name_id = ''
        error_codes.append('0')
        success_flag = 0



    try:
        name = request.form['name']
        if not (name):
            error_codes.append('1')
            success_flag = 0
    except:
        name = ''
        error_codes.append('1')
        success_flag = 0


    try:
        show = request.form['show']
        if not (show):
            error_codes.append('1')
            success_flag = 0
    except:
        show = ''
        error_codes.append('1')
        success_flag = 0


    try:
        rtl = True if request.form['rtl'] == 'true' else False
    except:
        rtl = False

    try:
        unicode = True if request.form['unicode'] == 'true' else False
    except:
        unicode = False

    try:
        type = request.form['type']
        if not (type):
            error_codes.append('2')
            success_flag = 0
    except:
        type = ''
        error_codes.append('2')
        success_flag = 0

    return name_id,name,show,rtl,unicode,type,error_codes,success_flag



#===========================================================================
#===========================================================================







#===========================================================================
#======================== validate_add_category ============================
#===========================================================================
def validate_update_category(request):
    error_codes = []
    warning_codes = []
    success_flag = 1
    code_length = 8

    try:
        input_data = json.loads(request.form['json'])
    except:
        input_data = {}


    try:
        input_metadata = json.loads(request.form['metadata'])
        cover = request.files['cover']




        # img = Image.open(cover)
        # print("========----------=========")
        # print(cover)
        # img.save( img.filename ,format="JPEG", quality=70)
        # print(img)
        # # cover = img
        # thumb_io = StringIO.StringIO()
        # thumb.save(thumb_io, format='JPEG')
        # cover = InMemoryUploadedFile(img, None, img.filename, 'image/jpeg',None)
    except:
        # raise
        input_metadata ={}
        cover = ""



    #-------------------------Validate the Details--------------------------

    try:
        hashtag_name = input_data['hashtag_name']
        if hashtag_name is None:
            error_codes.append('1')
            success_flag = 0

    except:
        hashtag_name = ''
        error_codes.append('1')
        success_flag = 0


    try:
        root_name = input_data['root_name']
    except:
        root_name = ''


    try:
        rootCover = input_data['rootCover']
    except:
        rootCover = ''



    try:
        title = input_data['title']
        # if note is None and this_type == 'setmedia':
        #     error_codes.append('1')
        #     success_flag = 0

    except:
        title = ''
        error_codes.append('1')
        success_flag = 0



    try:
        description = input_data['description']
        # if note is None and this_type == 'setmedia':
        #     error_codes.append('1')
        #     success_flag = 0

    except:
        description = ''
        error_codes.append('1')
        success_flag = 0



    try:
        for info in input_data['topics']:
            pass
        topics = input_data['topics']
        # if note is None and this_type == 'setmedia':
        #     error_codes.append('1')
        #     success_flag = 0

    except:
        topics = []
        error_codes.append('1')
        success_flag = 0





    return hashtag_name,title,description,topics,root_name,rootCover,cover,input_metadata,error_codes,success_flag



#===========================================================================
#===========================================================================



#===========================================================================
#========================= validate_user_login =============================
#===========================================================================
def validate_update_profile(request):
    error_codes = []
    warning_codes = []
    success_flag = 1

    try:
        input_data = request.json
    except:
        input_data = {}


    print(input_data)

    #-------------------------Validate the Details--------------------------

    try:
        username = input_data['username']
        if not (username):
            error_codes.append('2')
            success_flag = 0
    except:
        username = ''


    try:
        first_name = input_data['first_name']
        if not (first_name):
            error_codes.append('2')
            success_flag = 0
    except:
        first_name = ''


    try:
        last_name = input_data['last_name']
        if not (last_name):
            error_codes.append('2')
            success_flag = 0
    except:
        last_name = ''

    try:
        language = input_data['language']
        if not (language):
            error_codes.append('2')
            success_flag = 0
    except:
        language = ''

    try:
        Jlevel = int(input_data['Jlevel'])
        # if not (Jlevel):
        #     error_codes.append('2')
        #     success_flag = 0
    except:
        Jlevel = 0

    try:
        Wlevel = int(input_data['Wlevel'])
        # if not (WLevel):
        #     error_codes.append('2')
        #     success_flag = 0
    except:
        Wlevel = 0

    try:
        articles_langs = input_data['articles_langs']
        if not (articles_langs):
            error_codes.append('2')
            success_flag = 0
    except:
        articles_langs = []


    try:
        words_langs = input_data['words_langs']
        if not (words_langs):
            error_codes.append('2')
            success_flag = 0
    except:
        words_langs = []


    return username,first_name,last_name,language,Jlevel,Wlevel,articles_langs,words_langs,error_codes,success_flag



#===========================================================================
#===========================================================================




#===========================================================================
#=========================  validate_article  ==============================
#===========================================================================
def validate_article(request):
    error_codes = []
    warning_codes = []
    success_flag = 1

    try:
        input_data = request.json
    except:
        input_data = {}


    print(input_data)

    #-------------------------Validate the Details--------------------------

    try:
        hashtag_name = input_data['hashtag_name']
        if not (hashtag_name):
            error_codes.append('2')
            success_flag = 0
    except:
        hashtag_name = ''
        error_codes.append('2')
        success_flag = 0


    return hashtag_name,error_codes,success_flag



#===========================================================================
#===========================================================================



#===========================================================================
#=========================  validate_article  ==============================
#===========================================================================
def validate_word(request):
    error_codes = []
    warning_codes = []
    success_flag = 1

    try:
        input_data = request.json
    except:
        input_data = {}


    #-------------------------Validate the Details--------------------------

    try:
        word = input_data['word']
        if not (word):
            error_codes.append('2')
            success_flag = 0
    except:
        word = ''
        error_codes.append('2')
        success_flag = 0


    try:
        lang = input_data['lang']
        if not (lang):
            error_codes.append('2')
            success_flag = 0
    except:
        lang = ''
        error_codes.append('2')
        success_flag = 0


    return word,lang,error_codes,success_flag



#===========================================================================
#===========================================================================



#===========================================================================
#=========================  validate_article  ==============================
#===========================================================================
def validate_change_pass(request):
    error_codes = []
    warning_codes = []
    success_flag = 1

    try:
        input_data = request.json
    except:
        input_data = {}


    print(input_data)

    #-------------------------Validate the Details--------------------------

    try:
        username = input_data['username']
        if not (username):
            error_codes.append('2')
            success_flag = 0
    except:
        username = ''
        error_codes.append('2')
        success_flag = 0


    try:
        new_pass = input_data['new_pass']
        if not (new_pass):
            error_codes.append('2')
            success_flag = 0
    except:
        new_pass = ''
        error_codes.append('2')
        success_flag = 0


    return username,new_pass,error_codes,success_flag



#===========================================================================
#===========================================================================



#===========================================================================
#========================= validate_user_login =============================
#===========================================================================
def validate_add_page(request):
    error_codes = []
    warning_codes = []
    success_flag = 1

    try:
        input_data = request.json
    except:
        input_data = {}



    #-------------------------Validate the Details--------------------------

    try:
        lang = input_data['lang']
        # if note is None and this_type == '':
        #     lang = "CL",
    except:
        lang = 'CL'



    try:
        hashtag_name = input_data['hashtag_name']
        # if note is None and this_type == 'setmedia':
        #     error_codes.append('1')
        #     success_flag = 0

    except:
        hashtag_name = ''
        error_codes.append('1')
        success_flag = 0


    try:
        title = input_data['title']
        # if note is None and this_type == 'setmedia':
        #     error_codes.append('1')
        #     success_flag = 0

    except:
        title = ''
        error_codes.append('2')
        success_flag = 0



    try:
        main_description = input_data['main_description']
        # if note is None and this_type == 'setmedia':
        #     error_codes.append('1')
        #     success_flag = 0

    except:
        main_description = ''
        error_codes.append('3')
        success_flag = 0



    try:
        for info in input_data['content']:
            pass
        content = input_data['content']
        # if note is None and this_type == 'setmedia':
        #     error_codes.append('1')
        #     success_flag = 0

    except:
        content = []
        error_codes.append('4')
        success_flag = 0





    return lang,hashtag_name,title,main_description,content,error_codes,success_flag



#===========================================================================
#===========================================================================




#===========================================================================
#========================= validate_user_login =============================
#===========================================================================
def validate_page(request):
    error_codes = []
    warning_codes = []
    success_flag = 1

    try:
        input_data = request.json
    except:
        input_data = {}



    #-------------------------Validate the Details--------------------------

    try:
        hashtag_name = input_data['hashtag_name']
        if not (hashtag_name):
            error_codes.append('2')
            success_flag = 0
    except:
        hashtag_name = ''
        error_codes.append('2')
        success_flag = 0
    try:
        lang = input_data['lang']
        if not (lang):
            error_codes.append('2')
            success_flag = 0
    except:
        lang = ''
        error_codes.append('2')
        success_flag = 0
    
    return hashtag_name,lang,error_codes,success_flag



#===========================================================================
#===========================================================================
