from admin.translations.msgs import *
import flask_login

# LOCAL_LANG = DEFAULT_LANG


# ==============================================================================
# ==============================================================================
# ==============================================================================

def t(text_key):
    try:
        return MESSAGES[flask_login.current_user.language][text_key]
    except:
        return text_key
