from flask.views import MethodView
from flask import Flask, render_template, request, jsonify, redirect, url_for
from mongoengine.queryset.visitor import Q
from werkzeug.security import generate_password_hash, check_password_hash
from admin.models import *
from admin.validations import *
from admin.utils import *
from Shahi import *
from admin.translations.translations import *
import flask_login
import json
import os
from wiki_core.settings import UPLOAD_FOLDER


#===========================================================================
#===========================================================================






# ==========================================================================
# ==========================================================================
# ==========================================================================

class Login(MethodView):
    def post(self):
        username,password,error_codes,success_flag = validate_login(request)
        # flask.redirect(flask.url_for('operation'), code=307)
        data = {}
        if success_flag:
            current_user = User.objects(username=username).first()

            if current_user:
                if check_password_hash(current_user.password, password):
                    flask_login.login_user(current_user)
                    return redirect(url_for("admin_app.Main"))

            data['error_message'] = 'invalid username or password'


        data['login_error'] = True
        data['error_codes'] = error_codes
        return render_template('Login.html', data=data, t=t)
    def get(self):
        if flask_login.current_user.is_authenticated:
            return redirect(url_for("admin_app.Main"))

        data = {}
        data['login_error'] = False
        return render_template('Login.html', data=data, t=t)





# ==========================================================================
# ==========================================================================
# ==========================================================================
@login_manager.unauthorized_handler
def unauthorized_callback():
    return redirect('/admin/login')






# ==========================================================================
# ==========================================================================
# ==========================================================================
class Main(MethodView):
    decorators = [flask_login.login_required]
    def get(self):

        # print('==============')
        articles_num = Article.objects().count()
        users_num = User.objects().count()


        userData = {}
        userData['name'] = flask_login.current_user.first_name + ' ' + flask_login.current_user.last_name
        userData['user_name'] = flask_login.current_user.username
        userData['level'] = [0,0,0]
        for idx, value in enumerate(str(flask_login.current_user.level)[::-1]):
            userData['level'][idx] = int(value)
        userData['UI_Dir'] = 'ltr'
        if flask_login.current_user.language != 'CL':
            userData['UI_Dir'] = 'rtl'


        formData = {}
        formData['articles_num'] = articles_num
        formData['users_num'] = users_num

        return render_template('Main.html', userData=userData, formData=formData, t=t)




# ==========================================================================
# ==========================================================================
# ==========================================================================

class categoriesList(MethodView):

    decorators = [flask_login.login_required]
    def get(self):
        # article = Article.objects().all().delete()
        # print('dddddd')
        userData = {}
        userData['name'] = flask_login.current_user.first_name + ' ' + flask_login.current_user.last_name
        userData['user_name'] = flask_login.current_user.username
        userData['level'] = [0,0,0]
        for idx, value in enumerate(str(flask_login.current_user.level)[::-1]):
            userData['level'][idx] = int(value)
        userData['UI_Dir'] = 'ltr'
        if flask_login.current_user.language != 'CL':
            userData['UI_Dir'] = 'rtl'


        if flask_login.current_user.level == 999:
            exist_lang = ['CL', 'CLC', 'uCLC', '@LB', '@Y']
        else:
            exist_lang = [flask_login.current_user.language]


        formData = {}
        Categories_list = []
        formData['post'] = False
        formData['active_menu'] = 'categories'
        formData['active_submenu'] = 'cateList'

        formData['languages_list'] = []
        if int(str(flask_login.current_user.level)[::-1][0]) < 4:
            langs_list = Languages.objects(name_id__in=flask_login.current_user.articles_langs).all()
            if 'CL' in flask_login.current_user.articles_langs:
                langData= {}
                langData['name_id'] = 'CL'
                langData['name'] = 'YE@[LAY[T'
                langData['show'] = 'CALN[T'
                formData['languages_list'].append(langData)
            for lang in langs_list:
                langData = {}
                langData['name_id'] = lang.name_id
                langData['name'] = lang.name
                langData['show'] = lang.show
                langData['rtl'] = lang.rtl
                langData['unicode'] = lang.unicode
                formData['languages_list'].append(langData)
        else:
            langs_list = Languages.objects().all()
            langData= {}
            langData['name_id'] = 'CL'
            langData['name'] = 'YE@[LAY[T'
            langData['show'] = 'CALN[T'
            formData['languages_list'].append(langData)
            for lang in langs_list:
                langData = {}
                langData['name_id'] = lang.name_id
                langData['name'] = lang.name
                langData['show'] = lang.show
                langData['rtl'] = lang.rtl
                langData['unicode'] = lang.unicode
                formData['languages_list'].append(langData)

        Categories_list = Category.objects().all().order_by('-_id')


        return render_template('CategoriesList.html', userData=userData, formData=formData, Categories_list=Categories_list, t=t)



    def post(self):
        # ??????????????????
        userData = {}
        userData['name'] = flask_login.current_user.first_name + ' ' + flask_login.current_user.last_name
        userData['user_name'] = flask_login.current_user.username
        userData['level'] = [0,0,0]
        for idx, value in enumerate(str(flask_login.current_user.level)[::-1]):
            userData['level'][idx] = int(value)
        # ?????????????????
        userData['level'] = [0,0,0]
        for idx, value in enumerate(str(flask_login.current_user.level)[::-1]):
            userData['level'][idx] = int(value)
        userData['UI_Dir'] = 'ltr'
        if flask_login.current_user.language != 'CL':
            userData['UI_Dir'] = 'rtl'


        article_hashtag,type,error_codes,success_flag = validate_delete_article(request)




        if success_flag:
            formData = {}

            cate = Category.objects(hashtag_name=article_hashtag).first()
            if cate:
                if type == 'delete':
                    cate.delete()

                elif type == 'set_chosen':
                    date = ShahiDatetime.now().strftime('%Y%m%d%H%M%S')
                    cate.update(chosen=date)
                    cate.save()
                    print('ok')
                else:
                    formData['form_errors'] = ['sdasdasd']

            else:
                formData['form_errors'] = ['sdasdasd']

                # Article(hashtag_name=hashtag_name, title=title,author="test2", info_table=[],cover_url="", description=main_description,).save()
        else:
            formData['form_errors'] = ['sdasdasd']


        userData = {}
        userData['name'] = flask_login.current_user.first_name + ' ' + flask_login.current_user.last_name
        userData['user_name'] = flask_login.current_user.username
        userData['level'] = [0,0,0]
        for idx, value in enumerate(str(flask_login.current_user.level)[::-1]):
            userData['level'][idx] = int(value)
        userData['UI_Dir'] = 'ltr'
        if flask_login.current_user.language != 'CL':
            userData['UI_Dir'] = 'rtl'


        if flask_login.current_user.level == 999:
            exist_lang = ['CL', 'CLC', 'uCLC', '@LB', '@Y']
        else:
            exist_lang = [flask_login.current_user.language]


        formData = {}
        Categories_list = []
        formData['post'] = False
        formData['active_menu'] = 'categories'
        formData['active_submenu'] = 'cateList'

        formData['languages_list'] = []
        if int(str(flask_login.current_user.level)[::-1][0]) < 4:
            langs_list = Languages.objects(name_id__in=flask_login.current_user.articles_langs).all()
            if 'CL' in flask_login.current_user.articles_langs:
                langData= {}
                langData['name_id'] = 'CL'
                langData['name'] = 'YE@[LAY[T'
                langData['show'] = 'CALN[T'
                formData['languages_list'].append(langData)
            for lang in langs_list:
                langData = {}
                langData['name_id'] = lang.name_id
                langData['name'] = lang.name
                langData['show'] = lang.show
                langData['rtl'] = lang.rtl
                langData['unicode'] = lang.unicode
                formData['languages_list'].append(langData)
        else:
            langs_list = Languages.objects().all()
            langData= {}
            langData['name_id'] = 'CL'
            langData['name'] = 'YE@[LAY[T'
            langData['show'] = 'CALN[T'
            formData['languages_list'].append(langData)
            for lang in langs_list:
                langData = {}
                langData['name_id'] = lang.name_id
                langData['name'] = lang.name
                langData['show'] = lang.show
                langData['rtl'] = lang.rtl
                langData['unicode'] = lang.unicode
                formData['languages_list'].append(langData)

        Categories_list = Category.objects().all().order_by('-_id')

        return render_template('CategoriesList.html', userData=userData, formData=formData, Categories_list=Categories_list, t=t)




# ==========================================================================
# ==========================================================================
# ==========================================================================

class AddCategory(MethodView):
    decorators = [flask_login.login_required]
    def get(self):
        userData = {}
        userData['name'] = flask_login.current_user.first_name + ' ' + flask_login.current_user.last_name
        userData['user_name'] = flask_login.current_user.username
        userData['level'] = [0,0,0]
        for idx, value in enumerate(str(flask_login.current_user.level)[::-1]):
            userData['level'][idx] = int(value)
        userData['UI_Dir'] = 'ltr'
        if flask_login.current_user.language != 'CL':
            userData['UI_Dir'] = 'rtl'


        formData = {}
        formData['post'] = False
        formData['active_menu'] = 'categories'
        formData['active_submenu'] = 'addCate'
        formData['categories_list'] = Category.objects(lang='CL').all().order_by('-_hashtag_name')
        formData['languages_list'] = []
        if int(str(flask_login.current_user.level)[::-1][0]) < 4:
            langs_list = Languages.objects(name_id__in=flask_login.current_user.articles_langs).all()
            if 'CL' in flask_login.current_user.articles_langs:
                langData= {}
                langData['name_id'] = 'CL'
                langData['name'] = 'YE@[LAY[T'
                langData['show'] = 'CALN[T'
                formData['languages_list'].append(langData)
            for lang in langs_list:
                langData = {}
                langData['name_id'] = lang.name_id
                langData['name'] = lang.name
                langData['show'] = lang.show
                langData['rtl'] = lang.rtl
                langData['unicode'] = lang.unicode
                formData['languages_list'].append(langData)
        else:
            langs_list = Languages.objects().all()
            langData= {}
            langData['name_id'] = 'CL'
            langData['name'] = 'YE@[LAY[T'
            langData['show'] = 'CALN[T'
            formData['languages_list'].append(langData)
            for lang in langs_list:
                langData = {}
                langData['name_id'] = lang.name_id
                langData['name'] = lang.name
                langData['show'] = lang.show
                langData['rtl'] = lang.rtl
                langData['unicode'] = lang.unicode
                formData['languages_list'].append(langData)

        return render_template('AddCate.html', userData=userData, formData=formData, t=t)

    def post(self):

        userData = {}
        userData['name'] = flask_login.current_user.first_name + ' ' + flask_login.current_user.last_name
        userData['user_name'] = flask_login.current_user.username
        userData['level'] = [0,0,0]
        for idx, value in enumerate(str(flask_login.current_user.level)[::-1]):
            userData['level'][idx] = int(value)
        userData['UI_Dir'] = 'ltr'
        if flask_login.current_user.language != 'CL':
            userData['UI_Dir'] = 'rtl'


        formData = {}
        formData['post'] = True
        formData['active_menu'] = 'categories'
        formData['active_submenu'] = 'addCate'
        formData['categories_list'] = Category.objects(lang='CL').all().order_by('-_hashtag_name')
        formData['languages_list'] = []
        if int(str(flask_login.current_user.level)[::-1][0]) < 4:
            langs_list = Languages.objects(name_id__in=flask_login.current_user.articles_langs).all()
            if 'CL' in flask_login.current_user.articles_langs:
                langData= {}
                langData['name_id'] = 'CL'
                langData['name'] = 'YE@[LAY[T'
                langData['show'] = 'CALN[T'
                formData['languages_list'].append(langData)
            for lang in langs_list:
                langData = {}
                langData['name_id'] = lang.name_id
                langData['name'] = lang.name
                langData['show'] = lang.show
                langData['rtl'] = lang.rtl
                langData['unicode'] = lang.unicode
                formData['languages_list'].append(langData)
        else:
            langs_list = Languages.objects().all()
            langData= {}
            langData['name_id'] = 'CL'
            langData['name'] = 'YE@[LAY[T'
            langData['show'] = 'CALN[T'
            formData['languages_list'].append(langData)
            for lang in langs_list:
                langData = {}
                langData['name_id'] = lang.name_id
                langData['name'] = lang.name
                langData['show'] = lang.show
                langData['rtl'] = lang.rtl
                langData['unicode'] = lang.unicode
                formData['languages_list'].append(langData)

        lang,hashtag_name,title,description,topics,cover,input_metadata,error_codes,success_flag = validate_add_category(request)
        print(topics)
        print(lang)
        cover_name = ''
        if allowed_file(cover.filename):
            uplload_dir = os.path.join(UPLOAD_FOLDER)
            try:
                if not os.path.exists(uplload_dir):
                    os.makedirs(uplload_dir)
            except :
                print('eroorr make dir')

            cover_name = input_metadata['name']+".jpg"
            cover.save(os.path.join(UPLOAD_FOLDER, cover_name))

            file = open(os.path.join(UPLOAD_FOLDER, cover_name)+".metadata", "w+")
            file.write(json.dumps(input_metadata))
        else:
            pass




        if success_flag:

            cate = Category.objects(hashtag_name=hashtag_name, lang=lang)

            if cate:
                formData['form_errors'] = ['sdasdasd']
            else:
                formData['artName'] = 'lllll'
                date = ShahiDatetime.now().strftime('%Y%m%d%H%M%S')
                Category(hashtag_name=hashtag_name, title=title, author=flask_login.current_user.id, topics=topics,cover_url=cover_name, description=description, date=date, lang=lang, ).save()
        else:
            formData['form_errors'] = ['sdasdasd']

        return render_template('AddCate.html', userData=userData, formData=formData, t=t)






# ==========================================================================
# ==========================================================================
# ==========================================================================

class EditCategory(MethodView):
    decorators = [flask_login.login_required]
    def get(self, catename):
        userData = {}
        userData['name'] = flask_login.current_user.first_name + ' ' + flask_login.current_user.last_name
        userData['user_name'] = flask_login.current_user.username
        userData['level'] = [0,0,0]
        for idx, value in enumerate(str(flask_login.current_user.level)[::-1]):
            userData['level'][idx] = int(value)
        userData['UI_Dir'] = 'ltr'
        if flask_login.current_user.language != 'CL':
            userData['UI_Dir'] = 'rtl'


        formData = {}
        formData['active_menu'] = 'categories'
        formData['categories_list'] = Category.objects(lang='CL').all().order_by('-_hashtag_name')
        formData['languages_list'] = []
        if int(str(flask_login.current_user.level)[::-1][0]) < 4:
            langs_list = Languages.objects(name_id__in=flask_login.current_user.articles_langs).all()
            if 'CL' in flask_login.current_user.articles_langs:
                langData= {}
                langData['name_id'] = 'CL'
                langData['name'] = 'YE@[LAY[T'
                langData['show'] = 'CALN[T'
                formData['languages_list'].append(langData)
            for lang in langs_list:
                langData = {}
                langData['name_id'] = lang.name_id
                langData['name'] = lang.name
                langData['show'] = lang.show
                langData['rtl'] = lang.rtl
                langData['unicode'] = lang.unicode
                formData['languages_list'].append(langData)
        else:
            langs_list = Languages.objects().all()
            langData= {}
            langData['name_id'] = 'CL'
            langData['name'] = 'YE@[LAY[T'
            langData['show'] = 'CALN[T'
            formData['languages_list'].append(langData)
            for lang in langs_list:
                langData = {}
                langData['name_id'] = lang.name_id
                langData['name'] = lang.name
                langData['show'] = lang.show
                langData['rtl'] = lang.rtl
                langData['unicode'] = lang.unicode
                formData['languages_list'].append(langData)

        cate = Category.objects(hashtag_name=catename).first()

        if cate:

            if int(str(flask_login.current_user.level)[::-1][0]) < 4:
                if cate.lang not in flask_login.current_user.articles_langs:
                    return redirect('/admin/')


            # print(json.loads(article.to_json()))
            category_data = json.loads(cate.to_json())
            author = User.objects(id=category_data['author']['$oid']).first()
            category_data['author'] = author.first_name + ' ' + author.last_name
            category_data['author_username'] = author.username
            formData['category'] = category_data
            formData['post'] = False
        else:
            formData['category'] = {}
            formData['form_errors'] = ['sdasdasd']
            formData['post'] = True

        return render_template('EditCate.html', userData=userData, formData=formData, t=t)








# ==========================================================================
# ==========================================================================
# ==========================================================================

class Editor(MethodView):
    decorators = [flask_login.login_required]
    def get(self):
        article_list = Article.objects(lang='CL').all().order_by('-_id')

        userData = {}
        userData['name'] = flask_login.current_user.first_name + ' ' + flask_login.current_user.last_name
        userData['user_name'] = flask_login.current_user.username
        userData['level'] = [0,0,0]
        for idx, value in enumerate(str(flask_login.current_user.level)[::-1]):
            userData['level'][idx] = int(value)
        userData['UI_Dir'] = 'ltr'
        if flask_login.current_user.language != 'CL':
            userData['UI_Dir'] = 'rtl'


        formData = {}
        formData['post'] = False
        formData['active_menu'] = 'articles'
        formData['active_submenu'] = 'addArti'

        list = Category.objects().all()
        Categories_list = []
        for i in list:
            item = {}
            item['id'] = i.hashtag_name
            item['text'] = i.title
            item['lang'] = i.lang
            Categories_list.append(item)
        formData['Categories_list'] = Categories_list

        formData['languages_list'] = []

        if str(flask_login.current_user.level)[::-1][0] != '9':
            langs_list = Languages.objects(name_id__in=flask_login.current_user.articles_langs).all()
            if 'CL' in flask_login.current_user.articles_langs:
                langData= {}
                langData['name_id'] = 'CL'
                langData['name'] = 'YE@[LAY[T'
                langData['show'] = 'CALN[T'
                formData['languages_list'].append(langData)
            for lang in langs_list:
                langData = {}
                langData['name_id'] = lang.name_id
                langData['name'] = lang.name
                langData['show'] = lang.show
                langData['rtl'] = lang.rtl
                langData['unicode'] = lang.unicode
                formData['languages_list'].append(langData)
        else:
            langs_list = Languages.objects().all()
            langData= {}
            langData['name_id'] = 'CL'
            langData['name'] = 'YE@[LAY[T'
            langData['show'] = 'CALN[T'
            formData['languages_list'].append(langData)
            for lang in langs_list:
                langData = {}
                langData['name_id'] = lang.name_id
                langData['name'] = lang.name
                langData['show'] = lang.show
                langData['rtl'] = lang.rtl
                langData['unicode'] = lang.unicode
                formData['languages_list'].append(langData)

        return render_template('AddArticle.html', userData=userData, formData=formData, article_list=article_list, t=t)

    def post(self):
        article_list = Article.objects(lang='CL').all().order_by('-_id')

        userData = {}
        userData['name'] = flask_login.current_user.first_name + ' ' + flask_login.current_user.last_name
        userData['user_name'] = flask_login.current_user.username
        userData['level'] = [0,0,0]
        for idx, value in enumerate(str(flask_login.current_user.level)[::-1]):
            userData['level'][idx] = int(value)
        userData['UI_Dir'] = 'ltr'
        if flask_login.current_user.language != 'CL':
            userData['UI_Dir'] = 'rtl'


        formData = {}
        formData['post'] = True
        formData['active_menu'] = 'articles'
        formData['active_submenu'] = 'addArti'
        list = Category.objects().all()
        Categories_list = []
        for i in list:
            item = {}
            item['id'] = i.hashtag_name
            item['text'] = i.title
            item['lang'] = i.lang
            Categories_list.append(item)
        formData['Categories_list'] = Categories_list


        formData['languages_list'] = []

        if str(flask_login.current_user.level)[::-1][0] != '9':
            langs_list = Languages.objects(name_id__in=flask_login.current_user.articles_langs).all()
            if 'CL' in flask_login.current_user.articles_langs:
                langData= {}
                langData['name_id'] = 'CL'
                langData['name'] = 'YE@[LAY[T'
                langData['show'] = 'CALN[T'
                formData['languages_list'].append(langData)
            for lang in langs_list:
                langData = {}
                langData['name_id'] = lang.name_id
                langData['name'] = lang.name
                langData['show'] = lang.show
                langData['rtl'] = lang.rtl
                langData['unicode'] = lang.unicode
                formData['languages_list'].append(langData)
        else:
            langs_list = Languages.objects().all()
            langData= {}
            langData['name_id'] = 'CL'
            langData['name'] = 'YE@[LAY[T'
            langData['show'] = 'CALN[T'
            formData['languages_list'].append(langData)
            for lang in langs_list:
                langData = {}
                langData['name_id'] = lang.name_id
                langData['name'] = lang.name
                langData['show'] = lang.show
                langData['rtl'] = lang.rtl
                langData['unicode'] = lang.unicode
                formData['languages_list'].append(langData)

        lang,categories,hashtag_name,title,main_description,content,root_name,rootCover,cover,input_metadata,error_codes,success_flag = validate_add_article(request)

        if str(flask_login.current_user.level)[::-1][0] != '9':
            if lang not in flask_login.current_user.articles_langs:
                error_codes.append('23')
                success_flag = 0

        cover_name = ''
        if (cover):
            if allowed_file(cover.filename):
                uplload_dir = os.path.join(UPLOAD_FOLDER)
                try:
                    if not os.path.exists(uplload_dir):
                        os.makedirs(uplload_dir)
                except :
                    print('eroorr make dir')

                cover_name = input_metadata['name']+".jpg"
                cover.save(os.path.join(UPLOAD_FOLDER, cover_name))

                file = open(os.path.join(UPLOAD_FOLDER, cover_name)+".metadata", "w+")
                file.write(json.dumps(input_metadata))
            else:
                error_codes.append('24')
                success_flag = 0
        elif(rootCover):
            cover_name = rootCover


        if root_name:
            root_article = Article.objects(hashtag_name=root_name)
            if not root_article:
                error_codes.append('24')
                success_flag = 0

        if categories:
            for Hname in categories:
                cate = Category.objects(hashtag_name=Hname, lang=lang)
                if not cate:
                    error_codes.append('24')
                    success_flag = 0

        # TODO: check root for another link

        if success_flag:

            article = Article.objects(hashtag_name=hashtag_name)
            acceptable = 1

            if article:
                formData['form_errors'] = ['sdasdasd']
            else:

                if root_name:
                    root_article = Article.objects(hashtag_name=root_name)
                    set_before = Article.objects(root_name=root_name, lang=lang)
                    if not root_article:
                        error_codes.append('24')
                        acceptable = 0
                    if set_before and set_before != article:
                        error_codes.append('25')
                        acceptable = 0

                if acceptable:
                    formData['artName'] = 'lllll'
                    date = ShahiDatetime.now().strftime('%Y%m%d%H%M%S')
                    Article(hashtag_name=hashtag_name, title=title, root_name=root_name, category=categories,
                            author=flask_login.current_user.id, info_table=[],cover_url=cover_name,
                            description=main_description, date=date, lang=lang, content=content, ).save()
                else:
                    formData['form_errors'] = ['sdasdasd']

        else:
            formData['form_errors'] = ['sdasdasd']

        return render_template('AddArticle.html', userData=userData, formData=formData, article_list=article_list, t=t)




# ==========================================================================
# ==========================================================================
# ==========================================================================

class ArticleList(MethodView):

    decorators = [flask_login.login_required]
    def get(self):
        # article = Article.objects().all().delete()
        # print('dddddd')
        userData = {}
        userData['name'] = flask_login.current_user.first_name + ' ' + flask_login.current_user.last_name
        userData['user_name'] = flask_login.current_user.username
        userData['level'] = [0,0,0]
        for idx, value in enumerate(str(flask_login.current_user.level)[::-1]):
            userData['level'][idx] = int(value)
        userData['UI_Lang'] = flask_login.current_user.language
        userData['UI_Dir'] = 'ltr'
        if flask_login.current_user.language != 'CL':
            userData['UI_Dir'] = 'rtl'


        formData = {}
        formData['post'] = False
        formData['active_menu'] = 'articles'
        formData['active_submenu'] = 'articleList'
        formData['languages_list'] = []

        article_list = []
        exist_lang =[]
        langs_list = Languages.objects().all()

        if str(flask_login.current_user.level)[::-1][0] == '9':

            # first CL language
            langData= {}
            langData['name_id'] = 'CL'
            langData['name'] = 'YE@[LAY[T'
            langData['show'] = 'CALN[T'
            formData['languages_list'].append(langData)
            exist_lang.append('CL')

            for lang in langs_list:
                langData = {}
                langData['name_id'] = lang.name_id
                langData['name'] = lang.name
                langData['show'] = lang.show
                langData['rtl'] = lang.rtl
                langData['unicode'] = lang.unicode
                formData['languages_list'].append(langData)
                exist_lang.append(lang.name_id)

            article_list = Article.objects().all().order_by('-_id')
        else:
            # TODO: # XXX:
            exist_lang = flask_login.current_user.articles_langs
            if 'CL' in flask_login.current_user.articles_langs:
                langData= {}
                langData['name_id'] = 'CL'
                langData['name'] = 'YE@[LAY[T'
                langData['show'] = 'CALN[T'
                formData['languages_list'].append(langData)
                exist_lang.append('CL')
            for lang in langs_list:
                if lang.name_id in exist_lang:
                    langData = {}
                    langData['name_id'] = lang.name_id
                    langData['name'] = lang.name
                    langData['show'] = lang.show
                    langData['rtl'] = lang.rtl
                    langData['unicode'] = lang.unicode
                    formData['languages_list'].append(langData)

            # langData= {}
            # langData['name_id'] = 'CL'
            # langData['name'] = 'YE@[LAY[T'
            # formData['languages_list'].append(langData)
            # formData['languages_list'].append(flask_login.current_user.articles_langs[0])
            article_list = Article.objects(lang__in=flask_login.current_user.articles_langs).all().order_by('-_id')


        chosenArticle = {}
        for lang in exist_lang:
            # AR = Article.objects().filter( Q(lang=lang) or  Q(lang='u'+lang) ).order_by('-chosen').first()
            AR = Article.objects().filter( lang=lang ).order_by('-chosen').first()
            chosenArticle[lang] = AR
            # chosenArticle.append(AR)

        formData['chosen'] = chosenArticle




        return render_template('ArticleList.html', userData=userData, formData=formData, article_list=article_list, t=t)



    def post(self):


        article_hashtag,type,error_codes,success_flag = validate_delete_article(request)

        userData = {}

        userData['name'] = flask_login.current_user.first_name + ' ' + flask_login.current_user.last_name
        userData['user_name'] = flask_login.current_user.username
        userData['level'] = [0,0,0]
        for idx, value in enumerate(str(flask_login.current_user.level)[::-1]):
            userData['level'][idx] = int(value)
        userData['UI_Dir'] = 'ltr'
        if flask_login.current_user.language != 'CL':
            userData['UI_Dir'] = 'rtl'

        formData = {}
        formData['post'] = True
        formData['active_menu'] = 'articles'
        formData['active_submenu'] = 'articleList'
        formData['languages_list'] = []


        if success_flag:

            article = Article.objects(hashtag_name=article_hashtag).first()
            if article:
                # ==========================
                if type == 'delete':

                    if int(str(flask_login.current_user.level)[::-1][0]) >= 4:
                        article.delete()
                    elif int(str(flask_login.current_user.level)[::-1][0]) >= 2 and int(str(flask_login.current_user.level)[::-1][0]) < 4 :
                        if article.lang in flask_login.current_user.articles_langs:
                            article.delete()
                        else:
                            formData['form_errors'] = ['sdasdasd']
                    else:
                        formData['form_errors'] = ['sdasdasd']
                # ==========================
                elif type == 'set_chosen':
                    if int(str(flask_login.current_user.level)[::-1][0]) >= 4:
                        date = ShahiDatetime.now().strftime('%Y%m%d%H%M%S')
                        article.update(chosen=date)
                    elif int(str(flask_login.current_user.level)[::-1][0]) >= 2 and int(str(flask_login.current_user.level)[::-1][0]) < 4 :
                        if article.lang in flask_login.current_user.articles_langs:
                            date = ShahiDatetime.now().strftime('%Y%m%d%H%M%S')
                            article.update(chosen=date)
                        else:
                            formData['form_errors'] = ['sdasdasd']
                    else:
                        formData['form_errors'] = ['sdasdasd']
                # ==========================
                else:
                    formData['form_errors'] = ['sdasdasd']

            else:
                formData['form_errors'] = ['sdasdasd']

                # Article(hashtag_name=hashtag_name, title=title,author="test2", info_table=[],cover_url="", description=main_description,).save()
        else:
            formData['form_errors'] = ['sdasdasd']






        article_list = []
        exist_lang =[]
        langs_list = Languages.objects().all()

        if str(flask_login.current_user.level)[::-1][0] == '9':

            # first CL language
            langData= {}
            langData['name_id'] = 'CL'
            langData['name'] = 'YE@[LAY[T'
            langData['show'] = 'CALN[T'
            formData['languages_list'].append(langData)
            exist_lang.append('CL')

            for lang in langs_list:
                langData = {}
                langData['name_id'] = lang.name_id
                langData['name'] = lang.name
                langData['show'] = lang.show
                langData['rtl'] = lang.rtl
                langData['unicode'] = lang.unicode
                formData['languages_list'].append(langData)
                exist_lang.append(lang.name_id)

            article_list = Article.objects().all().order_by('-_id')
        else:
            # TODO: # XXX:
            exist_lang = flask_login.current_user.articles_langs
            if 'CL' in flask_login.current_user.articles_langs:
                langData= {}
                langData['name_id'] = 'CL'
                langData['name'] = 'YE@[LAY[T'
                langData['show'] = 'CALN[T'
                formData['languages_list'].append(langData)
                exist_lang.append('CL')
            for lang in langs_list:
                if lang.name_id in exist_lang:
                    langData = {}
                    langData['name_id'] = lang.name_id
                    langData['name'] = lang.name
                    langData['show'] = lang.show
                    langData['rtl'] = lang.rtl
                    langData['unicode'] = lang.unicode
                    formData['languages_list'].append(langData)
            article_list = Article.objects(lang__in=flask_login.current_user.articles_langs).all().order_by('-_id')


        chosenArticle = {}
        for lang in exist_lang:
            # AR = Article.objects().filter( Q(lang=lang) or  Q(lang='u'+lang) ).order_by('-chosen').first()
            AR = Article.objects().filter( lang=lang ).order_by('-chosen').first()
            chosenArticle[lang] = AR
            # chosenArticle.append(AR)

        formData['chosen'] = chosenArticle

        return render_template('ArticleList.html', userData=userData, formData=formData, article_list=article_list, t=t)








# ==========================================================================
# ==========================================================================
# ==========================================================================

class Update(MethodView):
    decorators = [flask_login.login_required]
    def get(self, articlename):



        userData = {}
        userData['name'] = flask_login.current_user.first_name + ' ' + flask_login.current_user.last_name
        userData['user_name'] = flask_login.current_user.username
        userData['level'] = [0,0,0]
        for idx, value in enumerate(str(flask_login.current_user.level)[::-1]):
            userData['level'][idx] = int(value)
        userData['UI_Dir'] = 'ltr'
        if flask_login.current_user.language != 'CL':
            userData['UI_Dir'] = 'rtl'

        formData = {}
        formData['active_menu'] = 'articles'
        formData['article_list'] = Article.objects(lang='CL').all().order_by('-_id')
        list = Category.objects().all()
        Categories_list = []
        for i in list:
            item = {}
            item['id'] = i.hashtag_name
            item['text'] = i.title
            item['lang'] = i.lang
            Categories_list.append(item)
        formData['Categories_list'] = Categories_list



        article = Article.objects(hashtag_name=articlename).first()

        if article:

            if int(str(flask_login.current_user.level)[::-1][0]) < 4:
                if article.lang not in flask_login.current_user.articles_langs:
                    return redirect('/admin/')


            # print(json.loads(article.to_json()))
            article_data = json.loads(article.to_json())
            author = User.objects(id=article_data['author']['$oid']).first()
            article_data['author'] = author.first_name + ' ' + author.last_name
            article_data['author_username'] = author.username

            formData['article'] = article_data
            formData['post'] = False
        else:
            formData['article'] = {}
            formData['form_errors'] = ['sdasdasd']
            formData['post'] = True

        return render_template('EditArticle.html', userData=userData, formData=formData, t=t)

    def post(self, articlename):
        formData = {}
        userData = {}
        userData['name'] = flask_login.current_user.first_name + ' ' + flask_login.current_user.last_name
        userData['user_name'] = flask_login.current_user.username
        userData['UI_Lang'] = flask_login.current_user.language
        userData['level'] = [0,0,0]
        for idx, value in enumerate(str(flask_login.current_user.level)[::-1]):
            userData['level'][idx] = int(value)
        userData['UI_Dir'] = 'ltr'
        if flask_login.current_user.language != 'CL':
            userData['UI_Dir'] = 'rtl'

        formData['post'] = True
        formData['active_menu'] = 'articles'
        formData['article_list'] = Article.objects(lang='CL').all().order_by('-_id')
        list = Category.objects().all()
        Categories_list = []
        for i in list:
            item = {}
            item['id'] = i.hashtag_name
            item['text'] = i.title
            item['lang'] = i.lang
            Categories_list.append(item)
        formData['Categories_list'] = Categories_list



        hashtag_name,title,categories,main_description,content,root_name,rootCover,cover,input_metadata,error_codes,success_flag = validate_update_article(request)


        if success_flag:

            article = Article.objects(hashtag_name=articlename).first()

            if article:
                acceptable = 1
                formData['article'] = json.loads(article.to_json())

                if (cover):
                    if allowed_file(cover.filename):
                        uplload_dir = os.path.join(UPLOAD_FOLDER)
                        try:
                            if not os.path.exists(uplload_dir):
                                os.makedirs(uplload_dir)
                        except :
                            print('eroorr make dir')

                        cover_name = input_metadata['name']+".jpg"
                        cover.save(os.path.join(UPLOAD_FOLDER, cover_name))

                        file = open(os.path.join(UPLOAD_FOLDER, cover_name)+".metadata", "w+")
                        file.write(json.dumps(input_metadata))
                    else:
                        error_codes.append('24')
                        acceptable = 0
                elif(rootCover):
                    cover_name = rootCover

                else:
                    cover_name = article.cover_url




                if root_name:
                    root_article = Article.objects(hashtag_name=root_name)
                    set_before = Article.objects(root_name=root_name, lang=article.lang)
                    if not root_article:
                        error_codes.append('24')
                        acceptable = 0
                    if set_before and not set_before != article:
                        error_codes.append('25')
                        acceptable = 0

                if categories:
                    for Hname in categories:
                        cate = Category.objects(hashtag_name=Hname, lang=article.lang)
                        if not cate:
                            error_codes.append('24')
                            acceptable = 0

                if acceptable:

                    date = ShahiDatetime.now().strftime('%Y%m%d%H%M%S')
                    # formData['form_errors'] = ['sdasdasd']

                    old_article = Old(author = article.author,info_table = article.info_table,
                        cover_url = article.cover_url,description = article.description,
                        tags = article.tags,content = article.content,category = article.category,
                        reference = article.reference )
                    article.old.append(old_article)
                    article.update(title=title,author=flask_login.current_user.id, info_table=[],
                                   cover_url=cover_name, date=date, content=content, root_name=root_name,
                                   description=main_description, category=categories)
                    # article.save()

                    updated_article = Article.objects(hashtag_name=articlename).first()
                    article_data = json.loads(article.to_json())
                    author = User.objects(id=article_data['author']['$oid']).first()
                    article_data['author'] = author.first_name + ' ' + author.last_name
                    article_data['author_username'] = author.username
                    formData['article'] = article_data
                    formData['success'] = True
                else:
                    formData['artName'] = 'not acceptable'
                    formData['success'] = False
            else:
                formData['artName'] = 'lllll'
                formData['success'] = False


        else:
            # formData['article'] = {}
            formData['form_errors'] = ['sdasdasd']
            formData['success'] = False

        return render_template('EditArticle.html', userData=userData, formData=formData, t=t)














# ==========================================================================
# ==========================================================================
# ==========================================================================

class AddWords(MethodView):
    decorators = [flask_login.login_required]
    def get(self):
        article_list = Article.objects(lang='CL').all().order_by('-_id')

        userData = {}
        userData['name'] = flask_login.current_user.first_name + ' ' + flask_login.current_user.last_name
        userData['user_name'] = flask_login.current_user.username
        userData['level'] = [0,0,0]
        for idx, value in enumerate(str(flask_login.current_user.level)[::-1]):
            userData['level'][idx] = int(value)
        userData['UI_Dir'] = 'ltr'
        if flask_login.current_user.language != 'CL':
            userData['UI_Dir'] = 'rtl'

        if int(str(flask_login.current_user.level)[::-1][1]) < 1:
            if article.lang not in flask_login.current_user.articles_langs:
                return redirect('/admin/')

        formData = {}
        formData['post'] = False
        formData['active_menu'] = 'words'
        formData['active_submenu'] = 'addword'

        list = Category.objects().all()
        Categories_list = []
        for i in list:
            item = {}
            item['id'] = i.hashtag_name
            item['text'] = i.title
            item['lang'] = i.lang
            Categories_list.append(item)
        formData['Categories_list'] = Categories_list

        formData['languages_list'] = []

        if str(flask_login.current_user.level)[::-1][0] != '9':
            langs_list = Languages.objects(name_id__in=flask_login.current_user.articles_langs).all()
            if 'CL' in flask_login.current_user.articles_langs:
                langData= {}
                langData['name_id'] = 'CL'
                langData['name'] = 'YE@[LAY[T'
                langData['show'] = 'CALN[T'
                formData['languages_list'].append(langData)
            for lang in langs_list:
                langData = {}
                langData['name_id'] = lang.name_id
                langData['name'] = lang.name
                langData['show'] = lang.show
                langData['rtl'] = lang.rtl
                langData['unicode'] = lang.unicode
                formData['languages_list'].append(langData)
        else:
            langs_list = Languages.objects().all()
            langData= {}
            langData['name_id'] = 'CL'
            langData['name'] = 'YE@[LAY[T'
            langData['show'] = 'CALN[T'
            formData['languages_list'].append(langData)
            for lang in langs_list:
                langData = {}
                langData['name_id'] = lang.name_id
                langData['name'] = lang.name
                langData['show'] = lang.show
                langData['rtl'] = lang.rtl
                langData['unicode'] = lang.unicode
                formData['languages_list'].append(langData)

        return render_template('AddWord.html', userData=userData, formData=formData, article_list=article_list, t=t)



# ==========================================================================
# ==========================================================================
# ==========================================================================


class WordsList(MethodView):

    decorators = [flask_login.login_required]
    def get(self):
        # article = Article.objects().all().delete()
        # print('dddddd')
        userData = {}
        userData['name'] = flask_login.current_user.first_name + ' ' + flask_login.current_user.last_name
        userData['user_name'] = flask_login.current_user.username
        userData['level'] = [0,0,0]
        for idx, value in enumerate(str(flask_login.current_user.level)[::-1]):
            userData['level'][idx] = int(value)
        userData['UI_Lang'] = flask_login.current_user.language
        userData['UI_Dir'] = 'ltr'
        if flask_login.current_user.language != 'CL':
            userData['UI_Dir'] = 'rtl'


        formData = {}
        formData['post'] = False
        formData['active_menu'] = 'words'
        formData['active_submenu'] = 'wordsList'
        formData['languages_list'] = []

        article_list = []
        exist_lang =[]
        langs_list = Languages.objects().all()

        if str(flask_login.current_user.level)[::-1][0] == '9':

            # first CL language
            langData= {}
            langData['name_id'] = 'CL'
            langData['name'] = 'YE@[LAY[T'
            langData['show'] = 'CALN[T'
            formData['languages_list'].append(langData)
            exist_lang.append('CL')

            for lang in langs_list:
                langData = {}
                langData['name_id'] = lang.name_id
                langData['name'] = lang.name
                langData['show'] = lang.show
                langData['rtl'] = lang.rtl
                langData['unicode'] = lang.unicode
                formData['languages_list'].append(langData)
                exist_lang.append(lang.name_id)

            words_list = Words.objects().all().order_by('-_id')
        else:
            # TODO: # XXX:
            exist_lang = flask_login.current_user.articles_langs
            if 'CL' in flask_login.current_user.articles_langs:
                langData= {}
                langData['name_id'] = 'CL'
                langData['name'] = 'YE@[LAY[T'
                langData['show'] = 'CALN[T'
                formData['languages_list'].append(langData)
                exist_lang.append('CL')
            for lang in langs_list:
                if lang.name_id in exist_lang:
                    langData = {}
                    langData['name_id'] = lang.name_id
                    langData['name'] = lang.name
                    langData['show'] = lang.show
                    langData['rtl'] = lang.rtl
                    langData['unicode'] = lang.unicode
                    formData['languages_list'].append(langData)

            # langData= {}
            # langData['name_id'] = 'CL'
            # langData['name'] = 'YE@[LAY[T'
            # formData['languages_list'].append(langData)
            # formData['languages_list'].append(flask_login.current_user.articles_langs[0])
            words_list = Words.objects(lang__in=flask_login.current_user.articles_langs).all().order_by('-_id')



        return render_template('WordsList.html', userData=userData, formData=formData, words_list=words_list, t=t)






# ==========================================================================
# ==========================================================================
# ==========================================================================

class EditWord(MethodView):
    decorators = [flask_login.login_required]
    def get(self, word):



        userData = {}
        userData['name'] = flask_login.current_user.first_name + ' ' + flask_login.current_user.last_name
        userData['user_name'] = flask_login.current_user.username
        userData['level'] = [0,0,0]
        for idx, value in enumerate(str(flask_login.current_user.level)[::-1]):
            userData['level'][idx] = int(value)
        userData['UI_Dir'] = 'ltr'
        if flask_login.current_user.language != 'CL':
            userData['UI_Dir'] = 'rtl'

        formData = {}
        formData['active_menu'] = 'words'
        # formData['article_list'] = Words.objects(lang='CL').all().order_by('-_id')

        formData['languages_list'] = []
        exist_lang =[]
        langs_list = Languages.objects().all()

        if str(flask_login.current_user.level)[::-1][0] == '9':

            # first CL language
            langData= {}
            langData['name_id'] = 'CL'
            langData['name'] = 'YE@[LAY[T'
            langData['show'] = 'CALN[T'
            formData['languages_list'].append(langData)
            exist_lang.append('CL')

            for lang in langs_list:
                langData = {}
                langData['name_id'] = lang.name_id
                langData['name'] = lang.name
                langData['show'] = lang.show
                langData['rtl'] = lang.rtl
                langData['unicode'] = lang.unicode
                formData['languages_list'].append(langData)
                exist_lang.append(lang.name_id)

            words_list = Words.objects().all().order_by('-_id')
        else:
            # TODO: # XXX:
            exist_lang = flask_login.current_user.articles_langs
            if 'CL' in flask_login.current_user.articles_langs:
                langData= {}
                langData['name_id'] = 'CL'
                langData['name'] = 'YE@[LAY[T'
                langData['show'] = 'CALN[T'
                formData['languages_list'].append(langData)
                exist_lang.append('CL')
            for lang in langs_list:
                if lang.name_id in exist_lang:
                    langData = {}
                    langData['name_id'] = lang.name_id
                    langData['name'] = lang.name
                    langData['show'] = lang.show
                    langData['rtl'] = lang.rtl
                    langData['unicode'] = lang.unicode
                    formData['languages_list'].append(langData)



        Word = Words.objects(word=word).first()

        if Word:

            if int(str(flask_login.current_user.level)[::-1][0]) < 4:
                if Word.lang not in flask_login.current_user.articles_langs:
                    return redirect('/admin/')


            # print(json.loads(article.to_json()))
            word_data = json.loads(Word.to_json())
            author = User.objects(id=word_data['author']['$oid']).first()
            word_data['author'] = author.first_name + ' ' + author.last_name
            word_data['author_username'] = author.username

            formData['Word'] = word_data
            formData['post'] = False
        else:
            formData['Word'] = {}
            formData['Word']['word_type'] = []
            formData['form_errors'] = ['sdasdasd']
            formData['post'] = False

        return render_template('EditWord.html', userData=userData, formData=formData, t=t)

# ==========================================================================
# ==========================================================================






# ==========================================================================
# ==========================================================================
# ==========================================================================
class AddUser(MethodView):
    decorators = [flask_login.login_required]
    def get(self):
        userData = {}
        userData['name'] = flask_login.current_user.first_name + ' ' + flask_login.current_user.last_name
        userData['user_name'] = flask_login.current_user.username
        userData['level'] = [0,0,0]
        for idx, value in enumerate(str(flask_login.current_user.level)[::-1]):
            userData['level'][idx] = int(value)
        userData['UI_Dir'] = 'ltr'
        if flask_login.current_user.language != 'CL':
            userData['UI_Dir'] = 'rtl'


        if userData['level'][0] !=9 and userData['level'][1] !=9 and userData['level'][2] !=9:
            return redirect('/admin/')



        formData = {}
        formData['post'] = False
        formData['active_menu'] = 'Profiles'
        formData['active_submenu'] = 'adduser'

        formData['languages_list'] = []
        langs_list = Languages.objects().all()
        langData= {}
        langData['name_id'] = 'CL'
        langData['name'] = 'YE@[LAY[T'
        langData['show'] = 'CALN[T'
        formData['languages_list'].append(langData)
        exist_lang =[]
        exist_lang.append('CL')
        for lang in langs_list:
            langData = {}
            langData['name_id'] = lang.name_id
            langData['name'] = lang.name
            langData['show'] = lang.show
            langData['rtl'] = lang.rtl
            langData['unicode'] = lang.unicode
            formData['languages_list'].append(langData)
        # article = Article.objects(hashtag_name=username).first()
        #
        # if article:
        #     print(json.loads(article.to_json()))
        #     formData['article'] = json.loads(article.to_json())
        #     formData['post'] = False
        # else:
        #     formData['article'] = {}
        #     formData['form_errors'] = ['sdasdasd']
        #     formData['post'] = True

        return render_template('AddUser.html', userData=userData, formData=formData, t=t)

    def post(self):
        formData = {}
        userData = {}
        userData['name'] = flask_login.current_user.first_name + ' ' + flask_login.current_user.last_name
        userData['user_name'] = flask_login.current_user.username
        userData['level'] = [0,0,0]
        for idx, value in enumerate(str(flask_login.current_user.level)[::-1]):
            userData['level'][idx] = int(value)
        userData['UI_Dir'] = 'ltr'
        if flask_login.current_user.language != 'CL':
            userData['UI_Dir'] = 'rtl'


        if userData['level'][0] !=9 and userData['level'][1] !=9 and userData['level'][2] !=9:
            return redirect('/admin/')


        formData['post'] = True
        formData['active_menu'] = 'Profiles'
        formData['active_submenu'] = 'adduser'
        formData['languages_list'] = []
        langs_list = Languages.objects().all()
        langData= {}
        langData['name_id'] = 'CL'
        langData['name'] = 'YE@[LAY[T'
        langData['show'] = 'CALN[T'
        formData['languages_list'].append(langData)
        exist_lang =[]
        exist_lang.append('CL')
        for lang in langs_list:
            langData = {}
            langData['name_id'] = lang.name_id
            langData['name'] = lang.name
            langData['show'] = lang.show
            langData['rtl'] = lang.rtl
            langData['unicode'] = lang.unicode
            formData['languages_list'].append(langData)

        username,FName,LName,lang,VLevel,JLevel,articles_allowed_lang,words_allowed_lang,password,error_codes,success_flag = validate_add_user(request)

        if success_flag:
            if userData['level'][0] !=9 and userData['level'][1] !=9 and userData['level'][2] !=9:
                return redirect('/admin/')



            exist_user = User.objects(username=username).first()

            if not exist_user:

                date = ShahiDatetime.now().strftime('%Y%m%d%H%M%S')
                # formData['form_errors'] = ['sdasdasd']

                password = generate_password_hash(password)
                recentDate = ShahiDatetime.now().strftime('%Y%m%d%H%M%S')
                level = int(VLevel + JLevel)

                User(email='', first_name=FName, last_name=LName, password=password, \
                     username=username, language=lang, level=level, create_date=recentDate, \
                     articles_langs=[articles_allowed_lang], words_langs=[words_allowed_lang],\
                     is_active=True,is_authenticated=True,is_anonymous=False).save()


                return redirect('/admin/profile_list')
                # formData['article'] = json.loads(updated_article.to_json())
                formData['post'] = True

            else:

                formData['artName'] = 'lllll'
                formData['form_errors'] = ['sdasdasd']


        else:
            # formData['article'] = {}
            formData['form_errors'] = ['sdasdasd']
            formData['post'] = True

        return render_template('AddUser.html', userData=userData, formData=formData, t=t)



# ==========================================================================
# ==========================================================================
# ==========================================================================






# ==========================================================================
# ==========================================================================
# ==========================================================================

class ProfilesList(MethodView):

    decorators = [flask_login.login_required]
    def get(self):
        # article = Article.objects().all().delete()
        # print('dddddd')
        userData = {}
        userData['name'] = flask_login.current_user.first_name + ' ' + flask_login.current_user.last_name
        userData['user_name'] = flask_login.current_user.username
        userData['level'] = [0,0,0]
        for idx, value in enumerate(str(flask_login.current_user.level)[::-1]):
            userData['level'][idx] = int(value)
        userData['UI_Dir'] = 'ltr'
        if flask_login.current_user.language != 'CL':
            userData['UI_Dir'] = 'rtl'

        print(userData['level'])
        if userData['level'][0] !=9 and userData['level'][1] !=9 and userData['level'][2] !=9:
            return redirect('/admin/')
# # TODO: # XXX: karbare level digar nabayad betavanad in ruye ra bebinad


        formData = {}
        profiles_list = []
        formData['post'] = False
        formData['active_menu'] = 'Profiles'
        formData['active_submenu'] = 'ProfilesList'

        if flask_login.current_user.level == 999:
            users_list = User.objects().all().order_by('-_create_date')
        else:
            if int(str(flask_login.current_user.level)[::-1][0]) ==9 :
                # users_list = User.objects().filter(Q(level__ne=999 ) or  Q(level__not__mod=(5, 0)) ).order_by('-_create_date')
                users_list = User.objects().filter(Q(level__ne=999 )  ).order_by('-_create_date')
            elif int(str(flask_login.current_user.level)[::-1][0]) ==9 :
                users_list = User.objects().filter(level__ne=999 ).order_by('-_create_date')
        for user in users_list:
            uData = {}
            uData['username'] = user.username
            uData['language'] = user.language
            uData['create_date'] = user.create_date
            uData['is_active'] = user.is_active
            uData['level'] = user.level
            uData['articles'] = Article.objects().filter(author=user.id ).order_by('title').count()
            profiles_list.append(uData)


        return render_template('ProfilesList.html', userData=userData, formData=formData, profiles_list=profiles_list, t=t)



    def post(self):
        # ??????????????????
        userData = {}
        userData['name'] = flask_login.current_user.first_name + ' ' + flask_login.current_user.last_name
        userData['user_name'] = flask_login.current_user.username
        userData['level'] = [0,0,0]
        for idx, value in enumerate(str(flask_login.current_user.level)[::-1]):
            userData['level'][idx] = int(value)
        userData['UI_Dir'] = 'ltr'
        if flask_login.current_user.language != 'CL':
            userData['UI_Dir'] = 'rtl'

        if userData['level'][0] !=9 and userData['level'][1] !=9 and userData['level'][2] !=9:
            return redirect('/admin/')

        article_hashtag,type,error_codes,success_flag = validate_delete_article(request)


        chosenArticle = Article.objects().order_by('title').first()

        formData = {}
        userData['level'] = [0,0,0]
        for idx, value in enumerate(str(flask_login.current_user.level)[::-1]):
            userData['level'][idx] = int(value)
        formData['post'] = True
        formData['chosen'] = chosenArticle

        if success_flag:

            article = Article.objects(hashtag_name=article_hashtag).first()

            if article:
                article.delete()
            else:
                formData['form_errors'] = ['sdasdasd']

                # Article(hashtag_name=hashtag_name, title=title,author="test2", info_table=[],cover_url="", description=main_description,).save()
        else:
            formData['form_errors'] = ['sdasdasd']


        userData = {}
        userData['name'] = flask_login.current_user.first_name + ' ' + flask_login.current_user.last_name
        userData['user_name'] = flask_login.current_user.username
        userData['level'] = [0,0,0]
        for idx, value in enumerate(str(flask_login.current_user.level)[::-1]):
            userData['level'][idx] = int(value)
        # formData = {}
        article_list = []
        # formData['post'] = False
        article_list = Article.objects().all().order_by('-_id')

        return render_template('ProfilesList.html', userData=userData, formData=formData, article_list=article_list, t=t)








# ==========================================================================
# ==========================================================================
# ==========================================================================



class Profile(MethodView):
    decorators = [flask_login.login_required]
    def get(self, username):
        article_list = []
        words_list = []
        userData = {}
        userData['name'] = flask_login.current_user.first_name + ' ' + flask_login.current_user.last_name
        userData['user_name'] = flask_login.current_user.username
        # userData['language'] = flask_login.current_user.language
        # userData['articles_langs'] = flask_login.current_user.articles_langs
        # userData['words_langs'] = flask_login.current_user.words_langs
        userData['level'] = [0,0,0]
        for idx, value in enumerate(str(flask_login.current_user.level)[::-1]):
            userData['level'][idx] = int(value)
        userData['UI_Dir'] = 'ltr'
        if flask_login.current_user.language != 'CL':
            userData['UI_Dir'] = 'rtl'

        formData = {}
        formData['active_menu'] = 'Profiles'
        formData['languages_list'] = []
        langs_list = Languages.objects().all()
        langData= {}
        langData['name_id'] = 'CL'
        langData['name'] = 'YE@[LAY[T'
        langData['show'] = 'CALN[T'
        formData['languages_list'].append(langData)
        for lang in langs_list:
            langData = {}
            langData['name_id'] = lang.name_id
            langData['name'] = lang.name
            langData['show'] = lang.show
            langData['rtl'] = lang.rtl
            langData['unicode'] = lang.unicode
            formData['languages_list'].append(langData)

        user_data = User.objects(username=username).first()

        if user_data:
            # print(json.loads(article.to_json()))
            formData['user_data'] = json.loads(user_data.to_json())
            formData['user_data']['level'] =  [int(i) for i in str(user_data.level)[::-1]]
            article_list = Article.objects(author=user_data).all().order_by('-_id')
            words_list = Words.objects(author=user_data).all().order_by('-_id')
            formData['post'] = False
            # print(article_list)
        else:
            formData['article'] = {}
            formData['form_errors'] = ['sdasdasd']
            formData['post'] = True

        return render_template('Profile.html', userData=userData, formData=formData, article_list=article_list, words_list=words_list, t=t)

    def post(self, username):
        formData = {}
        userData = {}
        userData['name'] = flask_login.current_user.first_name + ' ' + flask_login.current_user.last_name
        userData['user_name'] = flask_login.current_user.username
        userData['level'] = [0,0,0]
        for idx, value in enumerate(str(flask_login.current_user.level)[::-1]):
            userData['level'][idx] = int(value)
        userData['UI_Dir'] = 'ltr'
        if flask_login.current_user.language != 'CL':
            userData['UI_Dir'] = 'rtl'

        formData['post'] = True
        formData['languages_list'] = []
        langs_list = Languages.objects().all()
        langData= {}
        langData['name_id'] = 'CL'
        langData['name'] = 'YE@[LAY[T'
        langData['show'] = 'CALN[T'
        formData['languages_list'].append(langData)
        for lang in langs_list:
            langData = {}
            langData['name_id'] = lang.name_id
            langData['name'] = lang.name
            langData['show'] = lang.show
            langData['rtl'] = lang.rtl
            langData['unicode'] = lang.unicode
            formData['languages_list'].append(langData)

        hashtag_name,title,main_description,content,cover,input_metadata,error_codes,success_flag = validate_add_article(request)

        print(content)

        if success_flag:

            article = Article.objects(hashtag_name=articlename).first()

            if article:


                if allowed_file(cover.filename):
                    uplload_dir = os.path.join(UPLOAD_FOLDER)
                    try:
                        if not os.path.exists(uplload_dir):
                            os.makedirs(uplload_dir)
                    except :
                        print('eroorr make dir')

                    cover_name = input_metadata['name']+".jpg"
                    cover.save(os.path.join(UPLOAD_FOLDER, cover_name))
                else:
                    cover_name = article.cover_url

                if cover_name:
                    file = open(os.path.join(UPLOAD_FOLDER, cover_name)+".metadata", "w+")
                    file.write(json.dumps(input_metadata))

                date = ShahiDatetime.now().strftime('%Y%m%d%H%M%S')
                formData['form_errors'] = ['sdasdasd']

                old_article = Old(author = article.author,info_table = article.info_table,
                    cover_url = article.cover_url,description = article.description,
                    tags = article.tags,content = article.content,category = article.category,
                    reference = article.reference )
                article.old.append(old_article)
                article.update(title=title,author=flask_login.current_user.id, info_table=[],cover_url=cover_name, date=date, content=content, description=main_description)
                article.save()

                updated_article = Article.objects(hashtag_name=articlename).first()
                formData['article'] = json.loads(updated_article.to_json())
                formData['post'] = True
            else:
                formData['artName'] = 'lllll'

        else:
            # formData['article'] = {}
            formData['form_errors'] = ['sdasdasd']
            formData['post'] = True

        return render_template('Profile.html', userData=userData, formData=formData, t=t)



# ==========================================================================
# ==========================================================================
# ==========================================================================










# ==========================================================================
# ==========================================================================
# ==========================================================================

class languagesList(MethodView):

    decorators = [flask_login.login_required]
    def get(self):
        # article = Article.objects().all().delete()
        # print('dddddd')
        userData = {}
        userData['name'] = flask_login.current_user.first_name + ' ' + flask_login.current_user.last_name
        userData['user_name'] = flask_login.current_user.username
        userData['level'] = [0,0,0]
        for idx, value in enumerate(str(flask_login.current_user.level)[::-1]):
            userData['level'][idx] = int(value)
        userData['UI_Dir'] = 'ltr'
        if flask_login.current_user.language != 'CL':
            userData['UI_Dir'] = 'rtl'


        if flask_login.current_user.level == 999:
            exist_lang = ['CL', 'CLC', 'uCLC', '@LB', '@Y']
        else:
            exist_lang = [flask_login.current_user.language]



        formData = {}
        languages_list = []
        formData['post'] = False
        formData['active_menu'] = 'langsList'
        formData['CLdata'] = {}
        formData['CLdata']['articles'] = Article.objects().filter(lang='CL' ).count()
        formData['CLdata']['words'] = Words.objects().filter(lang='CL' ).count()
        formData['CLdata']['users'] = User.objects().filter(language='CL' ).count()

        langs_list = Languages.objects().all()
        for lang in langs_list:
            langData = {}
            langData['name_id'] = lang.name_id
            langData['name'] = lang.name
            langData['show'] = lang.show
            langData['rtl'] = lang.rtl
            langData['unicode'] = lang.unicode
            langData['create_date'] = lang.create_date
            langData['articles'] = Article.objects().filter(lang=lang.name_id ).count()
            langData['words'] = Words.objects().filter(lang=lang.name_id ).count()
            langData['users'] = User.objects().filter(language=lang.name_id ).count()
            languages_list.append(langData)


        return render_template('LangList.html', userData=userData, formData=formData, languages_list=languages_list, t=t)



    def post(self):
        # ??????????????????
        userData = {}
        userData['name'] = flask_login.current_user.first_name + ' ' + flask_login.current_user.last_name
        userData['user_name'] = flask_login.current_user.username
        userData['level'] = [0,0,0]
        for idx, value in enumerate(str(flask_login.current_user.level)[::-1]):
            userData['level'][idx] = int(value)
        userData['UI_Dir'] = 'ltr'
        if flask_login.current_user.language != 'CL':
            userData['UI_Dir'] = 'rtl'


        if flask_login.current_user.level == 999:
            exist_lang = ['CL', 'CLC', 'uCLC', '@LB', '@Y']
        else:
            exist_lang = [flask_login.current_user.language]

        # ?????????????????

        name_id,name,show,rtl,unicode,type,error_codes,success_flag = validate_langs_list(request)
        formData = {}
        formData['post'] = True


        if success_flag:
            lang = Languages.objects(name_id=name_id).first()
            if type == 'add':
                if lang:

                    formData['form_errors'] = ['sdasdasd']
                else:
                    date = ShahiDatetime.now().strftime('%Y%m%d%H%M%S')
                    Languages(name_id=name_id, name=name, show=show, rtl=rtl, unicode=unicode, create_date=date, is_active=True).save()
            elif type == 'edit':
                lang.update(name_id=name_id, name=name, show=show, rtl=rtl, unicode=unicode)
                lang.save()


                # Article(hashtag_name=hashtag_name, title=title,author="test2", info_table=[],cover_url="", description=main_description,).save()
        else:
            formData['form_errors'] = ['sdasdasd']



        userData['name'] = flask_login.current_user.first_name + ' ' + flask_login.current_user.last_name
        userData['user_name'] = flask_login.current_user.username

        formData = {}
        languages_list = []
        article_list = []
        formData['post'] = True
        formData['CLdata'] = {}
        formData['CLdata']['articles'] = Article.objects().filter(lang='CL' ).count()
        formData['CLdata']['users'] = User.objects().filter(language='CL' ).count()

        langs_list = Languages.objects().all()
        for lang in langs_list:
            langData = {}
            langData['name_id'] = lang.name_id
            langData['name'] = lang.name
            langData['show'] = lang.show
            langData['rtl'] = lang.rtl
            langData['unicode'] = lang.unicode
            langData['create_date'] = lang.create_date
            langData['articles'] = Article.objects().filter(lang=lang.name_id ).count()
            langData['users'] = User.objects().filter(language=lang.name_id ).count()
            languages_list.append(langData)
        return render_template('LangList.html', userData=userData, formData=formData, languages_list=languages_list, t=t)

#===========================================================================
#===========================================================================







# ==========================================================================
# ==========================================================================
# ==========================================================================

class AddPages(MethodView):
    decorators = [flask_login.login_required]
    def get(self):
        article_list = Article.objects(lang='CL').all().order_by('-_id')

        userData = {}
        userData['name'] = flask_login.current_user.first_name + ' ' + flask_login.current_user.last_name
        userData['user_name'] = flask_login.current_user.username
        userData['level'] = [0,0,0]
        for idx, value in enumerate(str(flask_login.current_user.level)[::-1]):
            userData['level'][idx] = int(value)
        userData['UI_Dir'] = 'ltr'
        if flask_login.current_user.language != 'CL':
            userData['UI_Dir'] = 'rtl'


        formData = {}
        formData['post'] = False
        formData['active_menu'] = 'Pages'
        formData['active_submenu'] = 'addPage'


        formData['languages_list'] = []

        if str(flask_login.current_user.level)[::-1][0] != '9':
            langs_list = Languages.objects(name_id__in=flask_login.current_user.articles_langs).all()
            if 'CL' in flask_login.current_user.articles_langs:
                langData= {}
                langData['name_id'] = 'CL'
                langData['name'] = 'YE@[LAY[T'
                langData['show'] = 'CALN[T'
                formData['languages_list'].append(langData)
            for lang in langs_list:
                langData = {}
                langData['name_id'] = lang.name_id
                langData['name'] = lang.name
                langData['show'] = lang.show
                langData['rtl'] = lang.rtl
                langData['unicode'] = lang.unicode
                formData['languages_list'].append(langData)
        else:
            langs_list = Languages.objects().all()
            langData= {}
            langData['name_id'] = 'CL'
            langData['name'] = 'YE@[LAY[T'
            langData['show'] = 'CALN[T'
            formData['languages_list'].append(langData)
            for lang in langs_list:
                langData = {}
                langData['name_id'] = lang.name_id
                langData['name'] = lang.name
                langData['show'] = lang.show
                langData['rtl'] = lang.rtl
                langData['unicode'] = lang.unicode
                formData['languages_list'].append(langData)

        return render_template('AddPage.html', userData=userData, formData=formData, article_list=article_list, t=t)
#===========================================================================
#===========================================================================



# ==========================================================================
# ==========================================================================
# ==========================================================================

class PagesList(MethodView):

    decorators = [flask_login.login_required]
    def get(self):
        # article = Article.objects().all().delete()
        # print('dddddd')
        userData = {}
        userData['name'] = flask_login.current_user.first_name + ' ' + flask_login.current_user.last_name
        userData['user_name'] = flask_login.current_user.username
        userData['level'] = [0,0,0]
        for idx, value in enumerate(str(flask_login.current_user.level)[::-1]):
            userData['level'][idx] = int(value)
        userData['UI_Lang'] = flask_login.current_user.language
        userData['UI_Dir'] = 'ltr'
        if flask_login.current_user.language != 'CL':
            userData['UI_Dir'] = 'rtl'


        formData = {}
        formData['post'] = False
        formData['active_menu'] = 'Pages'
        formData['active_submenu'] = 'PagesList'
        formData['languages_list'] = []

        pages_list = []
        exist_lang =[]
        langs_list = Languages.objects().all()

        if str(flask_login.current_user.level)[::-1][0] == '9':

            # first CL language
            langData= {}
            langData['name_id'] = 'CL'
            langData['name'] = 'YE@[LAY[T'
            langData['show'] = 'CALN[T'
            formData['languages_list'].append(langData)
            exist_lang.append('CL')

            for lang in langs_list:
                langData = {}
                langData['name_id'] = lang.name_id
                langData['name'] = lang.name
                langData['show'] = lang.show
                langData['rtl'] = lang.rtl
                langData['unicode'] = lang.unicode
                formData['languages_list'].append(langData)
                exist_lang.append(lang.name_id)

            pages_list = Pages.objects().all().order_by('-_id')
        else:
            # TODO: # XXX:
            exist_lang = flask_login.current_user.articles_langs
            if 'CL' in flask_login.current_user.articles_langs:
                langData= {}
                langData['name_id'] = 'CL'
                langData['name'] = 'YE@[LAY[T'
                langData['show'] = 'CALN[T'
                formData['languages_list'].append(langData)
                exist_lang.append('CL')
            for lang in langs_list:
                if lang.name_id in exist_lang:
                    langData = {}
                    langData['name_id'] = lang.name_id
                    langData['name'] = lang.name
                    langData['show'] = lang.show
                    langData['rtl'] = lang.rtl
                    langData['unicode'] = lang.unicode
                    formData['languages_list'].append(langData)

            # langData= {}
            # langData['name_id'] = 'CL'
            # langData['name'] = 'YE@[LAY[T'
            # formData['languages_list'].append(langData)
            # formData['languages_list'].append(flask_login.current_user.articles_langs[0])
            pages_list = Pages.objects(lang__in=flask_login.current_user.articles_langs).all().order_by('-_id')




        return render_template('PagesList.html', userData=userData, formData=formData, pages_list=pages_list, t=t)


#===========================================================================
#===========================================================================




# ==========================================================================
# ==========================================================================
# ==========================================================================

class EditPage(MethodView):
    decorators = [flask_login.login_required]
    def get(self, articlename):

        userData = {}
        userData['name'] = flask_login.current_user.first_name + ' ' + flask_login.current_user.last_name
        userData['user_name'] = flask_login.current_user.username
        userData['level'] = [0,0,0]
        for idx, value in enumerate(str(flask_login.current_user.level)[::-1]):
            userData['level'][idx] = int(value)
        userData['UI_Dir'] = 'ltr'
        if flask_login.current_user.language != 'CL':
            userData['UI_Dir'] = 'rtl'

        formData = {}
        formData['active_menu'] = 'Pages'

        Page = Pages.objects(hashtag_name=articlename).first()

        if Page:

            if int(str(flask_login.current_user.level)[::-1][0]) < 4:
                if Page.lang not in flask_login.current_user.articles_langs:
                    return redirect('/admin/')


            # print(json.loads(article.to_json()))
            page_data = json.loads(Page.to_json())
            last_editor = User.objects(id=page_data['last_editor']['$oid']).first()
            page_data['last_editor'] = last_editor.first_name + ' ' + last_editor.last_name
            page_data['editor_username'] = last_editor.username

            formData['Page'] = page_data
            formData['post'] = False
        else:
            formData['Page'] = {}
            formData['form_errors'] = ['sdasdasd']
            formData['post'] = True

        return render_template('EditPage.html', userData=userData, formData=formData, t=t)

#===========================================================================
#===========================================================================





# ==========================================================================
# ==========================================================================
# ==========================================================================

class logout(MethodView):
    def get(self):
        flask_login.logout_user()
        return redirect('/admin/login')


#===========================================================================
#===========================================================================






# ===============================================================================================
# ===============================================================================================
# ===============================================================================================



# ==========================================================================
# ==========================================================================
# ==========================================================================

class updateCategory(MethodView):
    decorators = [flask_login.login_required]

    def post(self):
        hashtag_name,title,description,topics,root_name,rootCover,cover,input_metadata,error_codes,success_flag = validate_update_category(request)

        if success_flag:

            cate = Category.objects(hashtag_name=hashtag_name).first()

            if cate:
                title = title if title else cate.title
                description = description if description else cate.description
                topics = topics if topics else cate.topics

                acceptable = 1

                if (cover):
                    if allowed_file(cover.filename):
                        uplload_dir = os.path.join(UPLOAD_FOLDER)
                        try:
                            if not os.path.exists(uplload_dir):
                                os.makedirs(uplload_dir)
                        except :
                            print('eroorr make dir')

                        cover_name = input_metadata['name']+".jpg"
                        cover.save(os.path.join(UPLOAD_FOLDER, cover_name))

                        file = open(os.path.join(UPLOAD_FOLDER, cover_name)+".metadata", "w+")
                        file.write(json.dumps(input_metadata))
                    else:
                        return jsonify({
                                "status" : "error",
                                "error_codes" : 404
                            }), 400
                elif(rootCover):
                    cover_name = rootCover

                else:
                    cover_name = cate.cover_url




                if root_name:
                    root_category = Category.objects(hashtag_name=root_name)
                    set_before = Category.objects(root_name=root_name, lang=cate.lang)
                    if not root_category:
                        return jsonify({
                                "status" : "error",
                                "error_codes" : 404
                            }), 400
                    if set_before and not set_before != cate:
                        return jsonify({
                                "status" : "error",
                                "error_codes" : 404
                            }), 400

                if acceptable:

                    date = ShahiDatetime.now().strftime('%Y%m%d%H%M%S')
                    # formData['form_errors'] = ['sdasdasd']

                    cate.update(title=title, author=flask_login.current_user.id,
                                topics=topics,cover_url=cover_name, description=description)



                    return jsonify({
                            "category" : True
                            }), 200


                return jsonify({
                        "status" : "error",
                        "error_codes" : 403
                    }), 400

            return jsonify({
                    "status" : "error",
                    "error_codes" : 404
                }), 400


        return jsonify({
                "status" : "error",
                "error_codes" : 403
            }), 400
# ==========================================================================
# ==========================================================================






# ==========================================================================
# ==========================================================================
# ==========================================================================

class RemoveArticle(MethodView):
    decorators = [flask_login.login_required]

    def post(self):
        article_hashtag,error_codes,success_flag = validate_article(request)

        if success_flag:

            article = Article.objects(hashtag_name=article_hashtag).first()
            if article:

                if int(str(flask_login.current_user.level)[::-1][0]) >= 4:
                    article.delete()
                    return jsonify({
                            "article" : True
                            }), 200
                elif int(str(flask_login.current_user.level)[::-1][0]) >= 2 and int(str(flask_login.current_user.level)[::-1][0]) < 4 :
                    if article.lang in flask_login.current_user.articles_langs:
                        article.delete()
                        return jsonify({
                                "article" : True
                                }), 200
                else:
                    return jsonify({
                            "status" : "error",
                            "error_codes" : 403
                        }), 400

                return jsonify({
                        "status" : "error",
                        "error_codes" : 404
                    }), 400


        return jsonify({
                "status" : "error",
                "error_codes" : 404
            }), 400
# ==========================================================================
# ==========================================================================





# ==========================================================================
# ==========================================================================
# ==========================================================================

class ChoosenArticle(MethodView):
    decorators = [flask_login.login_required]

    def post(self):
        article_hashtag,type,error_codes,success_flag = validate_delete_article(request)

        if success_flag:

            article = Article.objects(hashtag_name=article_hashtag).first()
            if article:

                if int(str(flask_login.current_user.level)[::-1][0]) >= 4:
                    date = ShahiDatetime.now().strftime('%Y%m%d%H%M%S')
                    article.update(chosen=date)
                    return jsonify({
                            "article" : True
                            }), 200
                elif int(str(flask_login.current_user.level)[::-1][0]) >= 2 and int(str(flask_login.current_user.level)[::-1][0]) < 4 :
                    if article.lang in flask_login.current_user.articles_langs:
                        date = ShahiDatetime.now().strftime('%Y%m%d%H%M%S')
                        article.update(chosen=date)
                        return jsonify({
                                "article" : True
                                }), 200

                return jsonify({
                        "status" : "error",
                        "error_codes" : 404
                    }), 400


        return jsonify({
                "status" : "error",
                "error_codes" : 404
            }), 400
# ==========================================================================
# ==========================================================================



# ==========================================================================
# ==========================================================================
# ==========================================================================

class AddWord(MethodView):
    decorators = [flask_login.login_required]

    def post(self):
        word_d,pronounce,lang,root_word,word_type,translations,replaces,error_codes,success_flag = validate_add_word(request)
        print(replaces)
        if success_flag:

            Word = Words.objects(word=word_d).first()

            if not Word:

                if int(str(flask_login.current_user.level)[::-1][2]) == 9 or int(str(flask_login.current_user.level)[::-1][1]) > 0:
                    date = ShahiDatetime.now().strftime('%Y%m%d%H%M%S')
                    try:
                        Words(word=word_d, pronounce=pronounce,  lang=lang, date=date,\
                             root_word=root_word, author=flask_login.current_user.id, word_type=word_type, \
                             translations=translations, replaces=replaces, verified=flask_login.current_user.id ).save()
                    except:
                        raise
                        return jsonify({
                                "status" : "error",
                                "error_codes" : 500
                            }), 400


                    return jsonify({
                            "status" : "ok",
                            "Word" : True
                            }), 200


                return jsonify({
                        "status" : "error",
                        "error_codes" : 403
                    }), 400


            return jsonify({
                    "status" : "error",
                    "error_codes" : 405
                }), 400


        return jsonify({
                "status" : "error",
                "error_codes" : 404
            }), 400





# ==========================================================================
# ==========================================================================




# ==========================================================================
# ==========================================================================
# ==========================================================================

class updateWord(MethodView):
    decorators = [flask_login.login_required]

    def post(self):
        word_d,verified,pronounce,lang,root_word,word_type,translations,replaces,error_codes,success_flag = validate_edit_word(request)

        if success_flag:

            Word = Words.objects(word=word_d, lang=lang).first()

            if Word:
                # lang not in flask_login.current_user.articles_langs # TODO:
                if int(str(flask_login.current_user.level)[::-1][2]) == 9 or int(str(flask_login.current_user.level)[::-1][1]) > 0:
                    date = ShahiDatetime.now().strftime('%Y%m%d%H%M%S')
                    try:
                        verification = None
                        print(verified)
                        if verified:
                            verification = flask_login.current_user.id
                        old_translate = Old_Translate(author = Word.author, pronounce=Word.pronounce,  root_word=Word.root_word,
                            word_type=Word.word_type, translations=Word.translations, replaces=Word.replaces )
                        # Word.old.append(old_translate)
                        Word.update(pronounce=pronounce,  root_word=root_word, verified=verification,
                                    author=flask_login.current_user.id, word_type=word_type,
                                    translations=translations, replaces=replaces)

                    except:
                        raise
                        return jsonify({
                                "status" : "error",
                                "error_codes" : 500
                            }), 400


                    return jsonify({
                            "status" : "ok",
                            "Word" : True
                            }), 200


                return jsonify({
                        "status" : "error",
                        "error_codes" : 403
                    }), 400


            return jsonify({
                    "status" : "error",
                    "error_codes" : 405
                }), 400


        return jsonify({
                "status" : "error",
                "error_codes" : 404
            }), 400





# ==========================================================================
# ==========================================================================




# ==========================================================================
# ==========================================================================
# ==========================================================================

class updateDictation(MethodView):
    decorators = [flask_login.login_required]

    def post(self):
        word_d,new_dictation,lang,error_codes,success_flag = validate_new_dictation(request)

        if success_flag:

            Word = Words.objects(word=word_d, lang=lang).first()

            if Word:

                if int(str(flask_login.current_user.level)[::-1][2]) == 9 or int(str(flask_login.current_user.level)[::-1][1]) > 0:
                    try:
                        old_translate = Old_Translate(author = Word.author, pronounce=Word.pronounce,  root_word=Word.root_word,
                            word_type=Word.word_type, translations=Word.translations, replaces=Word.replaces )
                        Word.old.append(old_translate)
                        Word.update(word=new_dictation)

                    except:
                        raise
                        return jsonify({
                                "status" : "error",
                                "error_codes" : 500
                            }), 400


                    return jsonify({
                            "status" : "ok",
                            "Word" : True
                            }), 200


                return jsonify({
                        "status" : "error",
                        "error_codes" : 403
                    }), 400


            return jsonify({
                    "status" : "error",
                    "error_codes" : 405
                }), 400


        return jsonify({
                "status" : "error",
                "error_codes" : 404
            }), 400





# ==========================================================================
# ==========================================================================



# ==========================================================================
# ==========================================================================
# ==========================================================================

class RemoveWord(MethodView):
    decorators = [flask_login.login_required]

    def post(self):
        word_d,lang,error_codes,success_flag = validate_word(request)

        if success_flag:

            Word = Words.objects(word=word_d, lang=lang).first()
            if Word:

                if int(str(flask_login.current_user.level)[::-1][1]) >= 4:
                    Word.delete()
                    return jsonify({
                            "word" : True
                            }), 200
                elif int(str(flask_login.current_user.level)[::-1][1]) >= 2 and int(str(flask_login.current_user.level)[::-1][1]) < 4 :
                    if Word.lang in flask_login.current_user.articles_langs:
                        Word.delete()
                        return jsonify({
                                "word" : True
                                }), 200
                else:
                    return jsonify({
                            "status" : "error",
                            "error_codes" : 403
                        }), 400

                return jsonify({
                        "status" : "error",
                        "error_codes" : 404
                    }), 400


        return jsonify({
                "status" : "error",
                "error_codes" : 404
            }), 400
# ==========================================================================
# ==========================================================================



# ==========================================================================
# ==========================================================================
# ==========================================================================

class updateProfile(MethodView):
    decorators = [flask_login.login_required]

    def post(self):
        username,first_name,last_name,language,Jlevel,Wlevel,articles_langs,words_langs,error_codes,success_flag = validate_update_profile(request)

        if success_flag:

            user = User.objects(username=username).first()

            if user:
            # if False:
                # username = username if username else user.username
                first_name = first_name if first_name else user.first_name
                last_name = last_name if last_name else user.last_name
                language = language if language else user.language
                articles_langs = articles_langs if articles_langs else user.articles_langs
                words_langs = words_langs if words_langs else user.words_langs

                current_user_level = [0,0,0]
                for idx, value in enumerate(str(flask_login.current_user.level)[::-1]):
                    current_user_level[idx] = int(value)

                if user.level != 999:
                    if current_user_level[0] == 9:
                        Jlevel = Jlevel
                    else:
                        Jlevel = current_user_level[0]

                    if current_user_level[1] == 9:
                        Wlevel = Wlevel
                    else:
                        Wlevel = current_user_level[1]

                    level = (Wlevel*10)+Jlevel
                else:
                    level = 999



                if int(str(flask_login.current_user.level)[::-1][0]) == 9 or int(str(flask_login.current_user.level)[::-1][1]) == 9:
                    user.update(first_name=first_name, last_name=last_name,\
                         username=username, language=language, level=level, \
                         articles_langs=articles_langs, words_langs=words_langs )
                    return jsonify({
                            "article" : True
                            }), 200
                else:
                    if flask_login.current_user.username == username :
                        user.update(first_name=first_name, last_name=last_name,\
                             username=username, language=language )
                        return jsonify({
                                "article" : True
                                }), 200

                    else:
                        return jsonify({
                                "status" : "error",
                                "error_codes" : 404
                            }), 400


                return jsonify({
                        "status" : "error",
                        "error_codes" : 404
                    }), 400


        return jsonify({
                "status" : "error",
                "error_codes" : 404
            }), 400
# ==========================================================================
# ==========================================================================






# ==========================================================================
# ==========================================================================
# ==========================================================================

class ChangePass(MethodView):
    decorators = [flask_login.login_required]

    def post(self):
        username,new_pass,error_codes,success_flag = validate_change_pass(request)
        print(username,new_pass,error_codes,success_flag)
        if success_flag:

            user = User.objects(username=username).first()

            if user:
            # if False:

                if flask_login.current_user.username == user.username :
                    password = generate_password_hash(new_pass)
                    user.update(password=password)
                    return jsonify({
                            "article" : True
                            }), 200

                else:
                    if int(str(flask_login.current_user.level)[::-1][0]) == 9:
                        password = generate_password_hash(new_pass)
                        user.update(password=password, is_authenticated = False)
                        return jsonify({
                                "article" : True
                                }), 200

                    jsonify({
                            "status" : "error",
                            "error_codes" : 403
                        }), 400


                return jsonify({
                        "status" : "error",
                        "error_codes" : 404
                    }), 400


        return jsonify({
                "status" : "error",
                "error_codes" : 404
            }), 400
# ==========================================================================
# ==========================================================================








# ==========================================================================
# ==========================================================================
# ==========================================================================

class AddPage(MethodView):
    decorators = [flask_login.login_required]

    def post(self):


        lang,hashtag_name,title,main_description,content,error_codes,success_flag = validate_add_page(request)
        print(error_codes)
        if success_flag:

            page = Pages.objects(hashtag_name=hashtag_name, lang=lang)

            if not page:

                if int(str(flask_login.current_user.level)[::-1][2]) == 9:
                    date = ShahiDatetime.now().strftime('%Y%m%d%H%M%S')
                    try:
                        Pages(hashtag_name=hashtag_name, title=title, date=date,
                              last_editor=flask_login.current_user.id, lang=lang,
                              description=main_description, content=content).save()
                    except:
                        return jsonify({
                                "status" : "error",
                                "error_codes" : 500
                            }), 400


                    return jsonify({
                            "status" : "ok",
                            "Word" : True
                            }), 200


                return jsonify({
                        "status" : "error",
                        "error_codes" : 403
                    }), 400


            return jsonify({
                    "status" : "error",
                    "error_codes" : 405
                }), 400


        return jsonify({
                "status" : "error",
                "error_codes" : 404
            }), 400





# ==========================================================================
# ==========================================================================




# ==========================================================================
# ==========================================================================
# ==========================================================================

class updatePage(MethodView):
    decorators = [flask_login.login_required]

    def post(self):
        lang,hashtag_name,title,main_description,content,error_codes,success_flag = validate_add_page(request)

        if success_flag:

            page = Pages.objects(hashtag_name=hashtag_name, lang=lang).first()

            if page:

                if int(str(flask_login.current_user.level)[::-1][2]) == 9:
                    date = ShahiDatetime.now().strftime('%Y%m%d%H%M%S')
                    try:
                        page.update(hashtag_name=hashtag_name, title=title, date=date,
                                    last_editor=flask_login.current_user.id, lang=lang,
                                    description=main_description, content=content)
                    except:
                        # raise
                        return jsonify({
                                "status" : "error",
                                "error_codes" : 500
                            }), 400


                    return jsonify({
                            "status" : "ok",
                            "page" : True
                            }), 200


                return jsonify({
                        "status" : "error",
                        "error_codes" : 403
                    }), 400


            return jsonify({
                    "status" : "error",
                    "error_codes" : 405
                }), 400


        return jsonify({
                "status" : "error",
                "error_codes" : 404
            }), 400





# ==========================================================================
# ==========================================================================


# ==========================================================================
# ==========================================================================
# ==========================================================================

class RemovePage(MethodView):
    decorators = [flask_login.login_required]

    def post(self):
        page_hashtag,lang,error_codes,success_flag = validate_page(request)

        if success_flag:

            page = Pages.objects(hashtag_name=page_hashtag, lang=lang).first()
            if page:

                if int(str(flask_login.current_user.level)[::-1][2]) == 9:
                    page.delete()
                    return jsonify({
                            "page" : True
                            }), 200
                else:
                    return jsonify({
                            "status" : "error",
                            "error_codes" : 403
                        }), 400

                return jsonify({
                        "status" : "error",
                        "error_codes" : 404
                    }), 400


        return jsonify({
                "status" : "error",
                "error_codes" : 404
            }), 400
# ==========================================================================
# ==========================================================================
