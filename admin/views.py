from flask import Blueprint
from admin.controls import *
import flask_login

admin_app = Blueprint('admin_app', __name__,  template_folder="templates", static_folder='admin_static')

# api_view =

admin_app.add_url_rule('/admin/login/', view_func=Login.as_view("Login"), methods=['GET', 'POST'])
admin_app.add_url_rule('/admin/', view_func=Main.as_view("Main"), methods=['GET',])
admin_app.add_url_rule('/admin/categories_list', view_func=categoriesList.as_view("categoriesList"), methods=['GET','POST'])
admin_app.add_url_rule('/admin/add_category', view_func=AddCategory.as_view("AddCategory"), methods=['GET','POST'])
admin_app.add_url_rule('/admin/category/<path:catename>', view_func=EditCategory.as_view("EditCategory"), methods=['GET'])
admin_app.add_url_rule('/admin/editor', view_func=Editor.as_view("Editor"), methods=['GET','POST'])
admin_app.add_url_rule('/admin/editor/<path:articlename>', view_func=Update.as_view("Update"), methods=['GET','POST'])
admin_app.add_url_rule('/admin/article_list', view_func=ArticleList.as_view("ArticleList"), methods=['GET','POST'])
admin_app.add_url_rule('/admin/profile/<path:username>', view_func=Profile.as_view("Profile"), methods=['GET','POST'])
admin_app.add_url_rule('/admin/add_words', view_func=AddWords.as_view("AddWords"), methods=['GET'])
admin_app.add_url_rule('/admin/words_list', view_func=WordsList.as_view("WordsList"), methods=['GET'])
admin_app.add_url_rule('/admin/word/<path:word>', view_func=EditWord.as_view("EditWord"), methods=['GET'])
admin_app.add_url_rule('/admin/add_user', view_func=AddUser.as_view("AddUser"), methods=['GET','POST'])
admin_app.add_url_rule('/admin/profile_list', view_func=ProfilesList.as_view("ProfilesList"), methods=['GET','POST'])
admin_app.add_url_rule('/admin/language_list', view_func=languagesList.as_view("languagesList"), methods=['GET','POST'])
# admin_app.add_url_rule('/admin/article_list', view_func=ArticleList.as_view("ArticleList"), methods=['GET','POST'])
admin_app.add_url_rule('/admin/add_page', view_func=AddPages.as_view("AddPages"), methods=['GET','POST'])
admin_app.add_url_rule('/admin/page/<path:articlename>', view_func=EditPage.as_view("EditPage"), methods=['GET','POST'])
admin_app.add_url_rule('/admin/pages_list', view_func=PagesList.as_view("PagesList"), methods=['GET','POST'])

admin_app.add_url_rule('/admin/logout', view_func=logout.as_view("logout"), methods=['GET'])



# ++++++++++++  Admin API +++++++++++++
admin_app.add_url_rule('/admin/api/update_category', view_func=updateCategory.as_view("updateCategory"), methods=['POST'])
admin_app.add_url_rule('/admin/api/remove_article', view_func=RemoveArticle.as_view("RemoveArticle"), methods=['POST'])
admin_app.add_url_rule('/admin/api/choosen_article', view_func=ChoosenArticle.as_view("ChoosenArticle"), methods=['POST'])
admin_app.add_url_rule('/admin/api/add_word', view_func=AddWord.as_view("AddWord"), methods=['POST'])
admin_app.add_url_rule('/admin/api/remove_word', view_func=RemoveWord.as_view("RemoveWord"), methods=['POST'])
admin_app.add_url_rule('/admin/api/update_word', view_func=updateWord.as_view("updateWord"), methods=['POST'])
admin_app.add_url_rule('/admin/api/update_dictation', view_func=updateDictation.as_view("updateDictation"), methods=['POST'])
admin_app.add_url_rule('/admin/api/update_profile', view_func=updateProfile.as_view("updateProfile"), methods=['POST'])
admin_app.add_url_rule('/admin/api/change_pass', view_func=ChangePass.as_view("ChangePass"), methods=['POST'])

admin_app.add_url_rule('/admin/api/add_page', view_func=AddPage.as_view("AddPage"), methods=['POST'])
admin_app.add_url_rule('/admin/api/remove_page', view_func=RemovePage.as_view("RemovePage"), methods=['POST'])
admin_app.add_url_rule('/admin/api/update_page', view_func=updatePage.as_view("updatePage"), methods=['POST'])
