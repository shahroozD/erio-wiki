from flask import Flask
# import plyvel
from wiki_core.shared import db, login_manager
# from flask_pymongo import PyMongo
from flask_mongoengine import MongoEngine
# import app
from api.views import api_app
from admin.views import admin_app
from web.views import web_app

#===========================================================================
#===========================================================================




def Wiki_app(**config_overrides):
    app = Flask(__name__)


    # Load config
    app.config.from_pyfile("settings.py")
    # apply overrides for tests
    app.config.update(config_overrides)

    # setup db
    db.init_app(app)

    login_manager.init_app(app)


    app.register_blueprint(api_app)
    app.register_blueprint(admin_app)
    app.register_blueprint(web_app)

    return app
