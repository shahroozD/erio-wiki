from werkzeug.security import generate_password_hash
from admin.models import User


def create_user():

    print('Enter Username:')
    username = input()

    exist_user = User.objects(username=username)

    if not exist_user:
        print('Enter first name:')
        FName = input()
        print('Enter last name:')
        LName = input()
        print('Enter User ui language:')
        # , unicod Avestan: uCLC, English: @Z, unicode English: u@Z
        print('-Persian: CL, Avestan: CLC' )
        Lang = input()

        print('Enter Password:')
        password = generate_password_hash(input())
        recentDate = ShahiDatetime.now().strftime('%Y%m%d%H%M%S')

        User(email='', first_name=FName, last_name=LName, password=password, \
             username=username, language=Lang, level=999, create_date=recentDate, \
             is_active=True,is_authenticated=True,is_anonymous=False).save()
    else:
        print('username exist! do you want update user Password?')
        print('Enter Password for Update:')
        password = generate_password_hash(input())

        print('level for Update:')
        level = int(input())

        exist_user.update(password=password, level=level)
        print('Password updated.')
