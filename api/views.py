from flask import Blueprint
from flask_cors import CORS
from api.controls import *

api_app = Blueprint('api_app', __name__)
CORS(api_app, resources={r"/api/*": {"origins": "*"}})
# api_view =

api_app.add_url_rule('/api/v1/home_page/', view_func=home_page.as_view("home_page"), methods=['POST',])
api_app.add_url_rule('/api/v1/article/', view_func=get_article.as_view("get_article"), methods=['POST',])
api_app.add_url_rule('/api/v1/search/', view_func=search.as_view("search"), methods=['POST',])
api_app.add_url_rule('/api/v1/word/', view_func=get_word.as_view("get_word"), methods=['POST',])
api_app.add_url_rule('/api/v1/page/', view_func=get_page.as_view("get_page"), methods=['POST',])


# ==================================================
api_app.add_url_rule('/media/<path:filename>', view_func=display_media)
