import json
from Shahi import *







#===========================================================================
#========================= validate_user_login =============================
#===========================================================================
def validate_login(request):
    error_codes = []
    warning_codes = []
    success_flag = 1


    #-------------------------Validate the Details--------------------------

    try:
        username = request.form['username']
        if not (username):
            error_codes.append('1')
            success_flag = 0
    except:
        username = ''
        error_codes.append('1')
        success_flag = 0



    try:
        password = request.form['password']
        if not (password):
            error_codes.append('1')
            success_flag = 0
    except:
        password = ''
        error_codes.append('1')
        success_flag = 0

    return username,password,error_codes,success_flag



#===========================================================================
#===========================================================================




#===========================================================================
#========================= validate_user_login =============================
#===========================================================================
def validate_add_article(request):
    error_codes = []
    warning_codes = []
    success_flag = 1
    code_length = 8

    try:
        input_data = json.loads(request.form['json'])
    except:
        input_data = {}


    try:
        input_metadata = json.loads(request.form['metadata'])
        cover = request.files['cover']




        # img = Image.open(cover)
        # print("========----------=========")
        # print(cover)
        # img.save( img.filename ,format="JPEG", quality=70)
        # print(img)
        # # cover = img
        # thumb_io = StringIO.StringIO()
        # thumb.save(thumb_io, format='JPEG')
        # cover = InMemoryUploadedFile(img, None, img.filename, 'image/jpeg',None)
    except:
        # raise
        input_metadata ={}
        cover = ""




    #-------------------------Validate the Details--------------------------

    try:
        hashtag_name = input_data['hashtag_name']
        # if note is None and this_type == 'setmedia':
        #     error_codes.append('1')
        #     success_flag = 0

    except:
        hashtag_name = ''
        error_codes.append('1')
        success_flag = 0


    try:
        name = input_data['name']
        # if note is None and this_type == 'setmedia':
        #     error_codes.append('1')
        #     success_flag = 0

    except:
        name = ''
        error_codes.append('1')
        success_flag = 0



    try:
        main_description = input_data['main_description']
        # if note is None and this_type == 'setmedia':
        #     error_codes.append('1')
        #     success_flag = 0

    except:
        main_description = ''
        error_codes.append('1')
        success_flag = 0



    try:
        for info in input_data['content']:
            pass
        content = input_data['content']
        # if note is None and this_type == 'setmedia':
        #     error_codes.append('1')
        #     success_flag = 0

    except:
        content = []
        error_codes.append('1')
        success_flag = 0





    return hashtag_name,name,main_description,content,cover,input_metadata,error_codes,success_flag



#===========================================================================
#===========================================================================



#===========================================================================
#========================= validate_add_class =============================
#===========================================================================
def validate_add_class(request):
    error_codes = []
    warning_codes = []
    success_flag = 1
    code_length = 8

    try:
        input_data = json.loads(request.POST)
    except:
        input_data = {}


    try:
        icon = request.FILES['icon']
    except:
        icon = ""


    #-------------------------Validate the Details--------------------------

    try:
        this_type = request.POST['type']

    except:
        this_type = ''

    try:
        name = request.POST['name']
        if name is None  :
            error_codes.append('1')
            success_flag = 0

    except:
        name = ''
        error_codes.append('1')
        success_flag = 0



    try:
        key = request.POST['key']
        if key is None  :
            error_codes.append('1')
            success_flag = 0

    except:
        key = ''
        error_codes.append('1')
        success_flag = 0




    return this_type,name,icon,key,error_codes,success_flag



#===========================================================================
#===========================================================================





#===========================================================================
#========================= validate_user_login =============================
#===========================================================================
def validate_delete_article(request):
    error_codes = []
    warning_codes = []
    success_flag = 1


    try:
        input_data = json.loads(request.body)
    except:
        input_data = {}

    #-------------------------Validate the Details--------------------------

    try:
        article_hashtag = request.form['article_hashtag']
        if not (article_hashtag):
            error_codes.append('1')
            success_flag = 0
    except:
        article_hashtag = ''
        error_codes.append('1')
        success_flag = 0



    try:
        type = request.form['type']
        if not (type):
            error_codes.append('1')
            success_flag = 0
    except:
        type = ''
        error_codes.append('1')
        success_flag = 0


    return article_hashtag,type,error_codes,success_flag



#===========================================================================
#===========================================================================









# ==========================================================================================================
# ==========================================================================================================
# ==========================================================================================================
# ==========================================================================================================






#===========================================================================
#========================= validate_user_login =============================
#===========================================================================
def validate_get_article(request):
    error_codes = []
    warning_codes = []
    success_flag = 1

    input_data = request.json

    # try:
    #     input_data = json.loads(request.body)
    # except:
    #     input_data = {}

    # print
    #-------------------------Validate the Details--------------------------
    try:
        articlename = input_data['articlename']
        if not (articlename):
            error_codes.append('1')
            success_flag = 0
    except:
        articlename = ''
        error_codes.append('1')
        success_flag = 0


    try:
        language = input_data['language']
        if not (language):
            language = 'CL'
            # error_codes.append('1')
            # success_flag = 0
    except:
        language = 'CL'
        # error_codes.append('1')
        # success_flag = 0


    return articlename,language,error_codes,success_flag



#===========================================================================
#===========================================================================




#===========================================================================
#========================= validate_user_login =============================
#===========================================================================
def validate_search(request):
    error_codes = []
    warning_codes = []
    success_flag = 1

    input_data = request.json

    # try:
    #     input_data = request.json
    # except:
    #     input_data = {}

    #-------------------------Validate the Details--------------------------

    try:
        search_keys = input_data['search_keys']
        if not (search_keys):
            error_codes.append('1')
            success_flag = 0
    except:
        search_keys = ''
        error_codes.append('1')
        success_flag = 0



    try:
        lang = input_data['language']
        if not (lang):
            lang = 'CL'
            # error_codes.append('1')
            # success_flag = 0
    except:
        lang = 'CL'
        # error_codes.append('1')
        # success_flag = 0


    return search_keys,lang,error_codes,success_flag



#===========================================================================
#===========================================================================





#===========================================================================
#========================= validate_user_login =============================
#===========================================================================
def validate_get_word(request):
    error_codes = []
    warning_codes = []
    success_flag = 1

    input_data = request.json

    # try:
    #     input_data = request.json
    # except:
    #     input_data = {}

    #-------------------------Validate the Details--------------------------

    try:
        search_keys = input_data['search_keys']
        if not (search_keys):
            error_codes.append('1')
            success_flag = 0
    except:
        search_keys = ''
        error_codes.append('1')
        success_flag = 0



    try:
        from_lang = input_data['from_lang']
        if not (from_lang):
            from_lang = 'CL'
            # error_codes.append('1')
            # success_flag = 0
    except:
        raise
        from_lang = 'CL'
        # error_codes.append('1')
        # success_flag = 0

    try:
        to_lang = input_data['to_lang']
        if not (to_lang):
            to_lang = 'CL'
            # error_codes.append('1')
            # success_flag = 0
    except:
        to_lang = 'CL'
        # error_codes.append('1')
        # success_flag = 0

    return search_keys,from_lang,to_lang,error_codes,success_flag



#===========================================================================
#===========================================================================



#===========================================================================
#========================= validate_user_login =============================
#===========================================================================
def validate_get_page(request):
    error_codes = []
    warning_codes = []
    success_flag = 1

    input_data = request.json

    # try:
    #     input_data = json.loads(request.body)
    # except:
    #     input_data = {}

    # print
    #-------------------------Validate the Details--------------------------
    try:
        hashtag_name = input_data['hashtag_name']
        if not (hashtag_name):
            error_codes.append('1')
            success_flag = 0
    except:
        hashtag_name = ''
        error_codes.append('2')
        success_flag = 0


    try:
        language = input_data['language']
        if not (language):
            language = 'CL'
            # error_codes.append('1')
            # success_flag = 0
    except:
        language = 'CL'
        # error_codes.append('1')
        # success_flag = 0


    return hashtag_name,language,error_codes,success_flag



#===========================================================================
#===========================================================================






#===========================================================================
#========================= validate_user_login =============================
#===========================================================================
def validate_home_page(request):
    error_codes = []
    warning_codes = []
    success_flag = 1

    input_data = request.json

    # try:
    #     input_data = request.json
    # except:
    #     input_data = {}

    #-------------------------Validate the Details--------------------------

    try:
        user_token = input_data['user_token']
        if not (user_token):
            user_token = None
    except:
        user_token = None



    try:
        lang = input_data['lang']
        if not (lang):
            lang = 'CL'
    except:
        lang = 'CL'



    return user_token,lang,error_codes,success_flag



#===========================================================================
#===========================================================================
