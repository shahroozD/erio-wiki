from flask.views import MethodView
from flask import jsonify, request, abort, send_from_directory
from mongoengine.queryset.visitor import Q as MQ
from bs4 import BeautifulSoup
from admin.models import *
from wiki_core.settings import UPLOAD_FOLDER
from api.validations import *

import re
# from api.decorators import *
# from api.utils import *
# from throne.default_vals import *
# from api.models import *
# from throne.new_calendar import *
# from sqlalchemy import or_
from Shahi import *
import json
#===========================================================================
#===========================================================================











# ==========================================================================
# ==========================================================================
# ==========================================================================

class home_page(MethodView):
    def post(self):
        search_keys,lang,error_codes,success_flag = validate_home_page(request)
        # flask.redirect(flask.url_for('operation'), code=307)
        # article = Article.objects().all().delete()
        articleData = {}
        if success_flag:

            best_article = Article.objects(lang=lang).order_by('-chosen').first()
            chosen ={}
            if best_article:
                bestData = json.loads(best_article.to_json())

                B_description_soup = BeautifulSoup(bestData['description'])
                for a in B_description_soup.findAll('a'):
                    a.replaceWithChildren()

                chosen['hashtag'] = bestData['hashtag_name']
                chosen['title'] = bestData['title']
                chosen['description'] = str(B_description_soup)
                chosen['cover_url'] = bestData['cover_url']



            article = Article.objects(lang=lang).limit(6);
            homeP_data ={}


            List =[]
            article_content = []

            for content in article:
                articleData = json.loads(content.to_json())

                description_soup = BeautifulSoup(articleData['description'])
                for a in description_soup.findAll('a'):
                    a.replaceWithChildren()

                content_description = {}
                content_description['hashtag'] = articleData['hashtag_name']
                content_description['title'] = articleData['title']
                content_description['description'] = str(description_soup)
                content_description['cover_url'] = articleData['cover_url']

            #     content_description = {}
            #     content_description['description'] = []
            #
            #     content_description['photos']=content['image']
            #     List.append(content['title'])
            #     content_description['title'] = content['title']
            #
            #
            #     split_text = re.split('<H1>|<H2>', content['text'])
            #     if len(split_text) > 1:
            #         sub_list =[List[-1]]
            #         for value in split_text:
            #             split_description = re.split('(^.*)(</H1>?|</H2>?)', value)
            #             print(split_description)
            #             second_description = {}
            #             if len(split_description) > 1 :
            #                 sub_list.append(split_description[1])
            #                 second_description['title']= split_description[1]
            #                 second_description['description']= split_description[3]
            #                 content_description['description'].append(second_description)
            #         # List.append(sub_list)
            #         List[-1] = sub_list
            #     else:
            #         second_description = {}
            #         second_description['title']= ''
            #         second_description['description']= split_text
            #
            #         content_description['description'].append(second_description)
            #
                article_content.append(content_description)



            homeP_data['list']= article_content
            homeP_data['chosen']= chosen
            #
            # AR['extra_links']= []


            return jsonify({
            "home_page" : homeP_data,
            "status" : "ok",
            }), 200

        return jsonify({
                "status" : "error",
                "error_codes" : 404
            }), 400







# ==========================================================================
# ==========================================================================
# ==========================================================================

class get_article(MethodView):
    def post(self):
        articlename,lang,error_codes,success_flag = validate_get_article(request)
        # flask.redirect(flask.url_for('operation'), code=307)
        # article = Article.objects().all().delete()
        articleData = {}
        if success_flag:
            print('===========================')
            print(articlename)
            article = Article.objects(hashtag_name=articlename, lang=lang).first()
            if article  :
                articleData = json.loads(article.to_json())

                AR ={}
                AR['cover_url'] = articleData['cover_url']
                AR['title'] = articleData['title']
                AR['description']= articleData['description']
                # AR['content_o']= articleData['content']
                AR['info_table']= articleData['info_table']
                AR['reference']= articleData['reference']
                AR['languages'] = []

                AR['Categories'] = {}
                AR['Categories']['name'] = ''
                AR['Categories']['template'] = []
                AR['Categories']['category'] = []
                AR['Categories']['baseInfo'] = []

                List =[]
                article_content = []

                for content in articleData['content']:
                    content_description = {}
                    content_description['description'] = []

                    content_description['photos']=content['image']
                    List.append(content['title'])
                    content_description['title'] = content['title']


                    split_text = re.split('<H1>|<H2>', content['text'])
                    if len(split_text) > 1:
                        sub_list =[List[-1]]
                        for value in split_text:
                            split_description = re.split('(^.*)(</H1>?|</H2>?)', value)
                            print(split_description)
                            second_description = {}
                            if len(split_description) > 1 :
                                sub_list.append(split_description[1])
                                second_description['title']= split_description[1]
                                second_description['description']= split_description[3]
                                content_description['description'].append(second_description)
                        # List.append(sub_list)
                        List[-1] = sub_list
                    else:
                        second_description = {}
                        second_description['title']= ''
                        second_description['description']= split_text

                        content_description['description'].append(second_description)

                    article_content.append(content_description)



                AR['content']= article_content
                AR['content_list']= List

                AR['extra_links']= []


                return jsonify({
                "article" : AR
                }), 200

            return jsonify({
                    "status" : "error",
                    "error_codes" : 404
                }), 400

        return jsonify({
                "status" : "error",
                "error_codes" : 405
            }), 400








# ==========================================================================
# ==========================================================================
# ==========================================================================

class search(MethodView):
    def post(self):
        search_keys,lang,error_codes,success_flag = validate_search(request)
        # flask.redirect(flask.url_for('operation'), code=307)
        # article = Article.objects().all().delete()
        articleData = {}
        if success_flag:
            # TODO: lang=lang MUST CHANGE
            hashtag_key = search_keys.replace(" ", "_")
            best_article = Article.objects(hashtag_name=hashtag_key, lang=lang).first()
            chosen ={}
            if best_article:
                bestData = json.loads(best_article.to_json())
                chosen['hashtag'] = bestData['hashtag_name']
                chosen['title'] = bestData['title']
                chosen['description'] = bestData['description']
                chosen['cover_url'] = [bestData['cover_url']]




            keys = search_keys.split(' ')

            articles_1 = Article.objects(description__icontains=search_keys, lang=lang)
            # articles_2 = Article.objects(description__icontains=search_keys)
            articlesData = json.loads(articles_1.to_json())


            Search_result ={}



            List =[]
            for content in articlesData:
                content_description = {}
                # content_description['description'] = []
                description_soup = BeautifulSoup(content['description'])
                for a in description_soup.findAll('a'):
                    a.replaceWithChildren()
                # content_description['photos']=content['image']
                # List.append(content['title'])
                content_description['hashtag'] = content['hashtag_name']
                content_description['title'] = content['title']
                content_description['description'] = str(description_soup)


            #     split_text = re.split('<H1>|<H2>', content['text'])
            #     if len(split_text) > 1:
            #         sub_list =[List[-1]]
            #         for value in split_text:
            #             split_description = re.split('(^.*)(</H1>?|</H2>?)', value)
            #             print(split_description)
            #             second_description = {}
            #             if len(split_description) > 1 :
            #                 sub_list.append(split_description[1])
            #                 second_description['title']= split_description[1]
            #                 second_description['description']= split_description[3]
            #                 content_description['description'].append(second_description)
            #         # List.append(sub_list)
            #         List[-1] = sub_list
            #     else:
            #         second_description = {}
            #         second_description['title']= ''
            #         second_description['description']= split_text
            #
            #         content_description['description'].append(second_description)
            #
                List.append(content_description)



            Search_result['list']= List
            Search_result['best_article']= chosen


            return jsonify({
            "search" : Search_result,
            "status" : "ok",
            }), 200

        return jsonify({
                "status" : "error",
                "error_codes" : 404
            }), 400




# ==========================================================================
# ==========================================================================
# ==========================================================================

class get_word(MethodView):
    def post(self):
        search_keys,from_lang,to_lang,error_codes,success_flag = validate_get_word(request)
        # flask.redirect(flask.url_for('operation'), code=307)
        # article = Article.objects().all().delete()
        articleData = {}
        bestData = {}
        if success_flag:
            # TODO: lang=lang MUST CHANGE
            # word_d = search_keys.replace(" ", "_")
            best_Word = Words.objects(word=search_keys, lang=from_lang).first()

            chosen ={}
            same_roots_list =[]

            if best_Word:
                bestData = json.loads(best_Word.to_json())
                chosen['word'] = bestData['word']
                chosen['pronounce'] = bestData['pronounce']
                chosen['type'] = bestData['word_type']
                chosen['root_word'] = bestData['root_word']
                chosen['translations'] = [trl for trl in bestData['translations'] if trl['lang'] == to_lang]


                same_roots = Words.objects(root_word=bestData['root_word'] if bestData['root_word'] else bestData['word'], lang=from_lang).all()
                if same_roots:
                    for same_root in same_roots:
                        content = json.loads(same_root.to_json())
                        if content['word'] != chosen['word']:
                            content_translations = {}
                            content_translations['word'] = content['word']
                            content_translations['pronounce'] = content['pronounce']
                            translations_soup =  [trl for trl in content['translations'] if trl['lang'] == to_lang]
                            content_translations['translations'] = translations_soup
                            same_roots_list.append(content_translations)

            else:
                bestData['word'] = search_keys




            keys = search_keys.split(' ')

            # articles_1 = Words.objects(description__icontains=search_keys, lang=from_lang, replaces__in=search_keys)
            other_words = Words.objects(MQ(word__icontains=search_keys, word__ne=bestData['word'], lang=from_lang)|MQ(replaces__in=search_keys, lang=from_lang))
            # articles_2 = Article.objects(description__icontains=search_keys)
            wordsData = json.loads(other_words.to_json())

            Search_result ={}

            List =[]
            for content in wordsData:
                content_translations = {}
                # # content_description['description'] = []
                # translations_soup = BeautifulSoup(content['translations'])
                translations_soup =  [trl for trl in content['translations'] if trl['lang'] == to_lang]
                content_translations['translations'] = translations_soup
                content_translations['word'] = content['word']
                content_translations['pronounce'] = content['pronounce']


            #     split_text = re.split('<H1>|<H2>', content['text'])
            #     if len(split_text) > 1:
            #         sub_list =[List[-1]]
            #         for value in split_text:
            #             split_description = re.split('(^.*)(</H1>?|</H2>?)', value)
            #             print(split_description)
            #             second_description = {}
            #             if len(split_description) > 1 :
            #                 sub_list.append(split_description[1])
            #                 second_description['title']= split_description[1]
            #                 second_description['description']= split_description[3]
            #                 content_description['description'].append(second_description)
            #         # List.append(sub_list)
            #         List[-1] = sub_list
            #     else:
            #         second_description = {}
            #         second_description['title']= ''
            #         second_description['description']= split_text
            #
            #         content_description['description'].append(second_description)
            #
                if translations_soup:
                    List.append(content_translations)


            if not List:
                other_words = Words.objects.filter(translations__match ={'meanings__icontains':search_keys, 'lang':to_lang}, lang=from_lang)
                # articles_2 = Article.objects(description__icontains=search_keys)
                wordsData = json.loads(other_words.to_json())

                for content in wordsData:
                    content_translations = {}
                    # # content_description['description'] = []
                    # translations_soup = BeautifulSoup(content['translations'])
                    translations_soup =  [trl for trl in content['translations'] if trl['lang'] == to_lang]
                    content_translations['translations'] = translations_soup
                    content_translations['word'] = content['word']
                    content_translations['pronounce'] = content['pronounce']
                    if translations_soup:
                        List.append(content_translations)


            Search_result['similar_list']= List
            Search_result['same_roots']= same_roots_list
            Search_result['best_word']= chosen


            return jsonify({
            "search" : Search_result,
            "status" : "ok",
            }), 200

        return jsonify({
                "status" : "error",
                "error_codes" : 404
            }), 400


# ==========================================================================
# ==========================================================================


# ==========================================================================
# ==========================================================================
# ==========================================================================

class get_page(MethodView):
    def post(self):
        hashtag_name,lang,error_codes,success_flag = validate_get_page(request)
        # flask.redirect(flask.url_for('operation'), code=307)
        # article = Article.objects().all().delete()
        pageData = {}
        if success_flag:
            page = Pages.objects(hashtag_name=hashtag_name, lang=lang).first()
            if page  :
                pageData = json.loads(page.to_json())

                AR ={}
                AR['cover_url'] = pageData['cover_url']
                # AR['hashtag_name'] = pageData['hashtag_name']
                AR['title'] = pageData['title']
                AR['description']= pageData['description']

                List =[]
                article_content = []

                for content in pageData['content']:
                    content_description = {}
                    content_description['description'] = []

                    content_description['photos']=content['image']
                    List.append(content['title'])
                    content_description['title'] = content['title']


                    split_text = re.split('<H1>|<H2>', content['text'])
                    if len(split_text) > 1:
                        sub_list =[List[-1]]
                        for value in split_text:
                            split_description = re.split('(^.*)(</H1>?|</H2>?)', value)
                            print(split_description)
                            second_description = {}
                            if len(split_description) > 1 :
                                sub_list.append(split_description[1])
                                second_description['title']= split_description[1]
                                second_description['description']= split_description[3]
                                content_description['description'].append(second_description)
                        # List.append(sub_list)
                        List[-1] = sub_list
                    else:
                        second_description = {}
                        second_description['title']= ''
                        second_description['description']= split_text

                        content_description['description'].append(second_description)

                    article_content.append(content_description)



                AR['content']= article_content
                AR['content_list']= List


                return jsonify({
                "article" : AR
                }), 200

            return jsonify({
                    "status" : "error",
                    "error_codes" : 404
                }), 400

        return jsonify({
                "status" : "error",
                "error_codes" : 405
            }), 400







# ==========================================================================
# ==========================================================================
# ==========================================================================
def display_media(filename):
    # image = Photo.query.filter_by(user_id=session['user_id'], filename=filename).first()
    if  True:
        return send_from_directory(UPLOAD_FOLDER, filename)
    else:
        return 'not allowed/found'
